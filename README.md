# INFORMAZIONI

Progetto di programmazione per il web per la gestione dei servizi sanitari provinciali.

Nome progetto: ProgWebOne.

Componenti del gruppo:
- Martintoni Matthias matricola 193299
- Sottovia Alessandro matricola 192225

Nella repository sono presenti:
- Documentazione
  - Documento e guida
  - JavaDoc (aprire index.html in Documentazione/JavaDoc/apidocs)
  - PDF con tutti i dati presenti nel database
- Copia del database
- Script per ricreare il database
- Progetto

> ###### Importante ######
> Sostituire il path del database dentro il package it.unitn.disi.wp.progwebone.jdbc in DBUtil.java nella stringa dburl