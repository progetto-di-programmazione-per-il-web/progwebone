CREATE TABLE SERVIZI_SANITARI_PROVINCIALI(
    NOME VARCHAR(41) NOT NULL,
    PROVINCIA VARCHAR(2) NOT NULL,
    E_MAIL VARCHAR(30) NOT NULL,
    PASSWORD VARCHAR(255) NOT NULL,
    ID INTEGER,
    PRIMARY KEY (PROVINCIA)
);

CREATE TABLE MEDICI(
    ID_MEDICO INTEGER NOT NULL,
    NOME VARCHAR(8) NOT NULL,
    COGNOME VARCHAR(10) NOT NULL,
    LUOGO_DI_RESIDENZA VARCHAR(11) NOT NULL,
    PROVINCIA VARCHAR(2) NOT NULL,
    E_MAIL VARCHAR(27) NOT NULL,
    PASSWORD VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID_MEDICO)
);

CREATE TABLE PAZIENTI(
    ID_PAZIENTE INTEGER NOT NULL,
    NOME VARCHAR(50) NOT NULL,
    COGNOME VARCHAR(50) NOT NULL,
    LUOGO_DI_NASCITA VARCHAR(10) NOT NULL,
    PROVINCIA VARCHAR(2) NOT NULL,
    CODICE_FISCALE VARCHAR(16) NOT NULL,
    SESSO VARCHAR(1) NOT NULL,
    MEDICO_DI_BASE INTEGER NOT NULL,
    E_MAIL VARCHAR(255) NOT NULL,
    DATA_DI_NASCITA VARCHAR(10) NOT NULL,
    PASSWORD VARCHAR(255) NOT NULL,
    AVATAR_PATH VARCHAR(255),
    ID_SERVIZIO INTEGER NOT NULL,
    PRIMARY KEY (ID_PAZIENTE),
    FOREIGN KEY (PROVINCIA) REFERENCES "SERVIZI_SANITARI_PROVINCIALI" ("PROVINCIA"),
    FOREIGN KEY (MEDICO_DI_BASE) REFERENCES "MEDICI" ("ID_MEDICO")
);

CREATE TABLE VISITE(
    ID_VISITA INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    ID_PAZIENTE INTEGER NOT NULL,
    ID_MEDICO INTEGER NOT NULL,
    VISITA VARCHAR(255) NOT NULL,
    ESAME_PRESCRITTO VARCHAR(255) NOT NULL,
    DATA_ORA TIMESTAMP,
    PAGATO VARCHAR(20),
    IMPORTO_PAGATO INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (ID_VISITA),
    FOREIGN KEY (ID_PAZIENTE) REFERENCES "PAZIENTI" ("ID_PAZIENTE"),
    FOREIGN KEY (ID_MEDICO) REFERENCES "MEDICI" ("ID_MEDICO")
);

CREATE TABLE RICETTE(
    ID_RICETTA INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    ID_PAZIENTE INTEGER NOT NULL,
    ID_MEDICO INTEGER NOT NULL,
    FARMACO VARCHAR(255) NOT NULL,
    DATA_ORA TIMESTAMP,
    PRIMARY KEY (ID_RICETTA),
    FOREIGN KEY (ID_PAZIENTE) REFERENCES "PAZIENTI" ("ID_PAZIENTE"),
    FOREIGN KEY (ID_MEDICO) REFERENCES "MEDICI" ("ID_MEDICO")
);

CREATE TABLE ESAMI(
    ID_ESAME INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY,
    ID_PAZIENTE INTEGER NOT NULL,
    ID_MEDICO INTEGER NOT NULL,
    ESAME VARCHAR(255) NOT NULL,
    ESITO VARCHAR(255) NOT NULL,
    DATA_ORA TIMESTAMP NOT NULL,
    PAGATO VARCHAR(20),
    IMPORTO_PAGATO INTEGER DEFAULT 0 NOT NULL,
    ESEGUITO INTEGER DEFAULT 0,
    PRIMARY KEY (ID_ESAME),
    FOREIGN KEY (ID_PAZIENTE) REFERENCES "PAZIENTI" ("ID_PAZIENTE"),
    FOREIGN KEY (ID_MEDICO) REFERENCES "MEDICI" ("ID_MEDICO")
);

CREATE TABLE ESAMI_POSSIBILI(
    ID_ESAME INTEGER NOT NULL,
    ESAME VARCHAR(255) NOT NULL,
    PRIMARY KEY (ID_ESAME)
);

CREATE TABLE NOTIFICHE(
    ID_PAZIENTE INTEGER NOT NULL,
    NUMERO_VISITE INTEGER NOT NULL,
    NUMERO_ESAMI INTEGER NOT NULL,
    NUMERO_RICETTE INTEGER NOT NULL,
    PRIMARY KEY (ID_PAZIENTE)
);

CREATE TABLE COOKIE(
    E_MAIL VARCHAR(255) NOT NULL,
    TOKEN VARCHAR(255) NOT NULL
);