package it.unitn.disi.wp.progwebone.utilityFunction;

import it.unitn.disi.wp.progwebone.entities.Ricetta;
import java.awt.Color;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;

/**
 * Classe per creare file .xls
 * 
 * @author ProgWebOne
 */
public class XlsMaker {

    /**
     *
     * @param campi
     * @param ricette
     * @param workbook
     * @param outputFile
     * @throws Exception
     */
    public void printWorkbook(List<String> campi, List<Ricetta> ricette, Workbook workbook, File outputFile) throws Exception {
        Sheet sheet = null;
        try {
            sheet = workbook.getSheetAt(0); //Recupero il foglio in cui scrivere 

        } catch (Exception iae) {
//Se il file Excel è nuovo, non esiste nessu foglio. Devo crearne uno.
            sheet = workbook.createSheet();
        }
        try {
            int row = 0;
            int column = 0;

            for (String record : campi) {
                printCell(
                        record, //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );
                column++;

            }

            row++;
            column = 0;

            for (Ricetta record : ricette) {
                printCell(
                        record.getId_ricetta(), //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );

                column++;
                printCell(
                        record.getId_paziente(), //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );

                column++;
                printCell(
                        record.getId_medico(), //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );

                column++;
                printCell(
                        record.getFarmaco(), //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );

                column++;

                printCell(
                        record.getTimetoString(), //L'oggetto da inserire nella cella
                        Color.WHITE, //Imposta il colore di sfondo della cella
                        row, //La riga in cui inserire l'oggetto
                        column, //La colonna in cui inserire l'oggetto
                        sheet //Il foglio in cui inserire l'oggetto
                );

                column = 0;
                row++;
            }
        } finally {
            final FileOutputStream fos;
            fos = new FileOutputStream(outputFile);
            workbook.write(fos);
            fos.close();
        }
    }

    /**
     *
     * @param ca
     * @param bgColor
     * @param row
     * @param col
     * @param sheet
     * @return
     */
    public Cell printCell(Object ca, Color bgColor, int row, int col, Sheet sheet) {

        Cell cell = printCell(ca, row, col, sheet);
        if (cell instanceof XSSFCell) {
            XSSFCell xssfCell = (XSSFCell) cell;
            XSSFCellStyle style = (XSSFCellStyle) sheet.getWorkbook().createCellStyle();

            style.setFillForegroundColor(new XSSFColor(bgColor));
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            xssfCell.setCellStyle(style);
        } else if (cell instanceof HSSFCell) {

            HSSFCell hssfCell = (HSSFCell) cell;
            CellStyle style = sheet.getWorkbook().createCellStyle();
            HSSFPalette palette = ((HSSFSheet) sheet).getWorkbook().getCustomPalette();
            HSSFColor color = palette.findSimilarColor((byte) bgColor.getRed(), (byte) bgColor.getGreen(), (byte) bgColor.getBlue());

            style.setFillForegroundColor(color.getIndex());
            hssfCell.setCellStyle(style);
        }
        return cell;
    }

    /**
     *
     * @param ca
     * @param row
     * @param col
     * @param sheet
     * @return
     */
    public Cell printCell(Object ca, int row, int col, Sheet sheet) {

        Row _row = sheet.getRow(row); //Provo a vedere se il foglio contiene già una

        if (_row == null) {
            _row = sheet.createRow(row); //Se il foglio non contiene la riga, la creo

        }
        final Cell cell = _row.createCell(col);
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

        if (ca instanceof Timestamp) {

            cell.setCellValue((Timestamp) ca);

            cellStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat("yyyy-mm-dd"));

        } else if (ca instanceof String) {

            cell.setCellValue((String) ca);
            if (((String) ca).contains("\n")) {
                cellStyle.setWrapText(true);
            }
        } else if (ca instanceof Number) {

            cell.setCellValue(((Number) ca).doubleValue());
        } else if (ca instanceof Boolean) {

            cell.setCellValue((Boolean) ca);
        }
        cell.setCellStyle(cellStyle);
        return cell;
    }
}
