package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.utilityFunction.Sha256;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Classe Util per gestire le connessioni al database
 *
 * @author ProgWebOne
 *
 */
public class DBUtil {
    
    final String dburl = "E:\\Desktop\\Progetto-web\\prog.db"; //Inserire qui il path assoluto del database
    
    Connection conn;
    
    /**
     * Funzione che carica i driver per apacher.derby e si connette al database
     */
    public void connetti() {

        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Impossible to find the JavaDB Driver: " + ex.getMessage());
            System.exit(1);
        }

        try {
            conn = DriverManager.getConnection("jdbc:derby:"+dburl);
        } catch (SQLException ex) {
            System.err.println("Impossible to connect to database: " + ex.getMessage());
            System.exit(2);
        }
    }

    /**
     * Funzione per chiudere la connessione
     */
    public void shutdown() {
        try {
            conn.close();
        } catch (SQLException sqle) {
            Logger.getLogger(DBUtil.class.getName()).info(sqle.getMessage());
        }
    }

    /**
     *
     * Funzione che controlla se email e password sono corrette in base al Tipo
     * reinderizza alla landing corretta
     *
     * @return True o False
     * @throws SQLException
     */
    public boolean Validation(String email, String password, int Tipo) throws SQLException {

        connetti();

        Sha256 enc = new Sha256();

        password = enc.encrypter(password);

        String sql = "";
        // tipo di query testate Select * From medici Where Password = 'password' and E_mail = 'pasquale.lucchesi@mail.com' 
        if (Tipo == 1) {
            sql = "Select * From pazienti Where Password = '" + password + "' and E_mail = '" + email + "'";
        }

        if (Tipo == 0) {
            sql = "Select * From medici Where Password = '" + password + "' and E_mail = '" + email + "'";
        }

        if (Tipo == 2) {
            sql = "Select * From servizi_sanitari_provinciali Where Password = '" + password + "' and E_mail = '" + email + "'";
        }

        try (PreparedStatement stm = conn.prepareStatement(sql)) {
            try (ResultSet rs = stm.executeQuery()) {
                boolean result = rs.next();
                shutdown(); // é assosulatamente vitale che lui sia dopo qualsiasi operazione sul ResultSet
                return result;
            }
        }
    }
    
    /**
     * Funzione che controlla se i dati nel cookie sono corretti
     * @param userName
     * @param token
     * @return bool
     * @throws SQLException 
     */
    public boolean checkRememberMe(String userName, String token) throws SQLException {
        connetti();
        String sql = "Select * From cookie where e_mail = '" + userName + "' and token = '" + token + "'";

        try (PreparedStatement stm = conn.prepareStatement(sql)) {
            try (ResultSet rs = stm.executeQuery()) {
                boolean result = rs.next();
                shutdown(); // é assosulatamente vitale che lui sia dopo qualsiasi operazione sul ResultSet
                return result;
            }
        }

    }
    
    /**
     * Funzione che inserisce nel database i contenuti del cookie
     * @param userName
     * @param token
     * @throws SQLException 
     */
    public void insertRememberMe(String userName, String token) throws SQLException {

        connetti();

        String sql = "insert into cookie ( e_mail, token) VALUES (?,?)";
        // create the sql insert preparedstatement
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setString(1, userName);
        preparedStmt.setString(2, token);

        preparedStmt.executeUpdate();

        shutdown();
    }
    
    /**
     * Funzione che rimuove dal database del cookie la riga corrispondente dopo il logout
     * @param userName
     * @param token
     * @throws SQLException 
     */
    public void DeleteRememberMe(String userName, String token) throws SQLException {

        connetti();

        String sql = "delete from cookie where e_mail = '" + userName + "' and token = '" + token + "'";
        PreparedStatement preparedStmt = conn.prepareStatement(sql);

        preparedStmt.executeUpdate();

        shutdown();
    }

}
