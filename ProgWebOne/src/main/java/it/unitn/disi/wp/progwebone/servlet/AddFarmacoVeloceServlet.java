package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import it.unitn.disi.wp.progwebone.utilityFunction.sendEmail;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  Servlet che gestisce l'aggiunta di farmaci, l'invio di mail e update di notifiche di tipo farmaco.
 *  Questa servlet necessita di qualche parametro in più rispetto a {@link it.unitn.disi.wp.progwebone.servlet.AddFarmacoServlet}
 * @author ProgWebOne
 */
public class AddFarmacoVeloceServlet extends HttpServlet {

    
    
    /**
     * 
     * Gestisce l'aggiunta di farmaci, l'invio di mail e update di notifiche di tipo farmaco.
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
    */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        Medico user = (Medico) request.getSession().getAttribute("user");

        String farmaco = request.getParameter("Farmaco");
        String nome = request.getParameter("Nome");
        String cognome = request.getParameter("Cognome");
        String codice_fiscale = request.getParameter("Codice_Fiscale");
        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilRicette dbRicette = new DBUtilRicette();

        Paziente paziente = new Paziente();

        try {
            paziente = dbPaziente.ValidatePaziente(nome, cognome, codice_fiscale);
            if (user.getId_medico() == paziente.getMedicoDiBase()) {
                dbRicette.InserisciFarmaco(paziente.getId(), user.getId_medico(), farmaco);
                dbNotifiche.updateNotifiche(paziente.getId(), 0, 0, 1);
                sendEmail.send(paziente.getEmail(), "Ti è stato prescritto un nuovo farmaco");
                request.getSession().setAttribute("codice", 2);
            }else{
                request.getSession().setAttribute("codice", 1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect(response.encodeRedirectURL(contextPath + "LandingMedico"));

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
