package it.unitn.disi.wp.progwebone.entities;

/**
 * Rappresenta i dati di un paziente
 *
 * @author ProgWebOne
 *
 */
public class Paziente {

    private int id_paziente;
    private String nome;
    private String cognome;
    private String sesso;
    private String data_di_nascita;
    private String luogo_di_nascita;
    private String codice_fiscale;
    private String e_mail;
    private int medico_di_base;
    private String provincia;
    private String password;
    private String avatar_path;
    private int id_servizio;

    /**
     * Dati:
     *
     * @param id_paziente rappresenta id del paziente
     * @param nome rappresenta nome del paziente
     * @param cognome rappresenta il cognome
     * @param sesso rappresenta il sesso
     * @param data_di_nascita rappresenta la data di nascita
     * @param luogo_di_nascita rappresenta il luogo di nascita
     * @param e_mail rappresenta la email
     * @param medico_di_base rappresenta id del medico di base
     * @param provincia rappresenta la provincia di appartenenza
     * @param password rappresenta la password
     * @param avatar_path rappresenta il path per la foto profilo
     * @param id_servizio id servizio sanitario a cui è associato
     */
    /**
     * Metodo getter
     *
     * @return id_servizio
     */
    public int getId_servizio() {
        return id_servizio;
    }

    /**
     * Metodo setter
     * @param id_servizio
     */
    public void setId_servizio(int id_servizio) {
        this.id_servizio = id_servizio;
    }

    /**
     * Metodo setter
     * @return avatar_path
     */
    public String getAvatar_path() {
        return avatar_path;
    }

    /**
     * Metodo setter
     * @param avatar_path
     */
    public void setAvatar_path(String avatar_path) {
        this.avatar_path = avatar_path;
    }

    /**
     * Costruttore vuoto
     */
    public Paziente() {

    }

    /**
     * Inizializzo il paziente con :
     *
     * @param e_mail
     * @param pass
     */
    public Paziente(String e_mail, String pass) {
        this.e_mail = e_mail;
        this.password = pass;
    }

    /**
     *
     * @param id_paziente
     * @param nome
     * @param cognome
     * @param sesso
     * @param data_di_nascita
     * @param luogo_di_nascita
     * @param codice_fiscale
     * @param e_mail
     * @param medico_di_base
     * @param provincia
     * @param password
     * @param avatar_path
     */
    public Paziente(int id_paziente, String nome, String cognome, String sesso, String data_di_nascita, String luogo_di_nascita, String codice_fiscale, String e_mail, int medico_di_base, String provincia, String password, String avatar_path) {
        this.id_paziente = id_paziente;
        this.nome = nome;
        this.cognome = cognome;
        this.sesso = sesso;
        this.data_di_nascita = data_di_nascita;
        this.luogo_di_nascita = luogo_di_nascita;
        this.codice_fiscale = codice_fiscale;
        this.e_mail = e_mail;
        this.medico_di_base = medico_di_base;
        this.provincia = provincia;
        this.password = password;
        this.avatar_path = avatar_path;
    }

    /**
     * Metodo setter
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo setter
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo setter
     * @param id_paziente
     */
    public void setId_paziente(int id_paziente) {
        this.id_paziente = id_paziente;
    }

    /**
     * Metodo setter
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo setter
     * @param cognome
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * Metodo setter
     * @param sesso
     */
    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    /**
     * Metodo setter
     * @param data_di_nascita
     */
    public void setData_di_nascita(String data_di_nascita) {
        this.data_di_nascita = data_di_nascita;
    }

    /**
     * Metodo setter
     * @param luogo_di_nascita
     */
    public void setLuogo_di_nascita(String luogo_di_nascita) {
        this.luogo_di_nascita = luogo_di_nascita;
    }

    /**
     * Metodo setter
     * @param codice_fiscale
     */
    public void setCodice_fiscale(String codice_fiscale) {
        this.codice_fiscale = codice_fiscale;
    }

    /**
     * Metodo setter
     * @param e_mail
     */
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    /**
     * Metodo setter
     * @param medico_di_base
     */
    public void setMedico_di_base(int medico_di_base) {
        this.medico_di_base = medico_di_base;
    }

    /**
     * Metodo setter
     * @param provincia
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    //metodi Getter
    /**
     * Metodo getter
     *
     * @return id_paziente
     */
    public int getId() {
        return id_paziente;
    }

    /**
     * Metodo getter
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo getter
     *
     * @return cognome
     */
    public String getCognome() {
        return cognome;
    }

    /**
     * Metodo getter
     *
     * @return data_di_nascita
     */
    public String getDataDiNascita() {
        return data_di_nascita;
    }

    /**
     * Metodo getter
     *
     * @return luogo_di_nascita
     */
    public String getLuogoDiNascita() {
        return luogo_di_nascita;
    }

    /**
     * Metodo getter
     *
     * @return codice_fiscale
     */
    public String getCodiceFiscale() {
        return codice_fiscale;
    }

    /**
     * Metodo getter
     *
     * @return e_mail
     */
    public String getEmail() {
        return e_mail;
    }

    /**
     * Metodo getter
     *
     * @return sesso
     */
    public String getSesso() {
        return sesso;
    }

    /**
     * Metodo getter
     *
     * @return medico_di_base
     */
    public int getMedicoDiBase() {
        return medico_di_base;
    }

    /**
     * Metodo getter
     *
     * @return provincia
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     *
     *
     * Metodo To string
     *
     * @return ritorna l'oggetto in formato string
     */
    @Override
    public String toString() {

        return "Paziente [id: " + id_paziente + " Nome: " + nome + " Cognome: " + cognome + "Sesso: " + sesso + " Data di nascita: " + data_di_nascita + " Luogo di nascita: " + luogo_di_nascita + " Codice fiscale: " + codice_fiscale + " email: " + e_mail + " Provincia: " + provincia + " Password: " + password + " ]\n";
    }

}
