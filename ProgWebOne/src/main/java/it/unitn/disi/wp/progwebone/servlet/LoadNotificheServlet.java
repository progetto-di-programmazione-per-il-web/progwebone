package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Notifiche;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede le notifiche specifiche di un paziente
 *
 * @author ProgWebOne
 */
public class LoadNotificheServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoadNotifiche(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoadNotifiche(request, response);
    }

    /** 
     * Richiede le notifiche specifiche di un paziente
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void LoadNotifiche(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        Notifiche notifica = new Notifiche();

        try {
            notifica = dbNotifiche.getNotifiche(user.getId());
        } catch (SQLException ex) {
            Logger.getLogger(LoadNotificheServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        request.getSession().setAttribute("notifica", notifica);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "landingPaziente.jsp"));

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
