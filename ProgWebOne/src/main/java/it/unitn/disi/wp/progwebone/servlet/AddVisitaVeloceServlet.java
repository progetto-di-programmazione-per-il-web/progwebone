package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.EsamePossibile;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsamiPossibili;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import it.unitn.disi.wp.progwebone.utilityFunction.sendEmail;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  Servlet che gestisce l'aggiunta di visite, l'invio di mail e update di notifiche di tipo visita.
 *  Questa servlet necessita di qualche parametro in più rispetto a {@link it.unitn.disi.wp.progwebone.servlet.AddVisitaServlet}
 * @author ProgWebOne
 */
public class AddVisitaVeloceServlet extends HttpServlet {

    /**
     * 
     * Gestisce l'aggiunta di visite, l'invio di mail e update di notifiche di tipo visita.
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
    */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        Medico user = (Medico) request.getSession().getAttribute("user");

        String visita = request.getParameter("Visita");
        int id_esame = Integer.parseInt(request.getParameter("Esame_prescritto"));
        String nome = request.getParameter("Nome");
        String cognome = request.getParameter("Cognome");
        String codice_fiscale = request.getParameter("Codice_Fiscale");
        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilEsamiPossibili dbEsamiPossibili = new DBUtilEsamiPossibili();
        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();
        String Esame_Prescritto = "Nessuno";
        List<EsamePossibile> esami_possibili;
        esami_possibili = new ArrayList<>();

        try {
            esami_possibili = dbEsamiPossibili.getEsamiPossibili();
        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (id_esame != -1) {
            for (EsamePossibile esaTmp : esami_possibili) {
                if (id_esame == esaTmp.getId_esame()) {
                    Esame_Prescritto = esaTmp.getEsame();
                }
            }
        }

        Paziente paziente = new Paziente();

        try {
            paziente = dbPaziente.ValidatePaziente(nome, cognome, codice_fiscale);
            if (user.getId_medico() == paziente.getMedicoDiBase()) {
                dbVisite.InserisciVisita(paziente.getId(), user.getId_medico(), visita, Esame_Prescritto);
                dbNotifiche.updateNotifiche(paziente.getId(), 1, 0, 0);
                sendEmail.send(paziente.getEmail(), "Ti è stato eseguita una nuova visita");
                if (id_esame != -1) {
                    dbEsami.InserisciEsame(paziente.getId(), user.getId_medico(), Esame_Prescritto);
                    dbNotifiche.updateNotifiche(paziente.getId(), 0, 1, 0);
                    sendEmail.send(paziente.getEmail(), "Ti è stato prescritto un nuovo esame");
                }
                request.getSession().setAttribute("codice", 2);
            }else{
                request.getSession().setAttribute("codice", 1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect(response.encodeRedirectURL(contextPath + "LandingMedico"));

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
