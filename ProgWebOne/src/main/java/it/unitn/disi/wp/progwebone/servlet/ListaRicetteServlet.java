package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Notifiche;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet che richiede la lista delle ricette di uno specifico paziente
 * @author ProgWebOne
 */
public class ListaRicetteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaRicette(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaRicette(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    /**
     * Richiede la lista delle ricette di uno specifico paziente e la inserisce nella request
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void listaRicette(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Ricetta> ricette = new ArrayList<>();

        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilRicette dbRicette = new DBUtilRicette();

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        Notifiche notifica = null;

        try {
            ricette = dbRicette.getRicette(user.getId());
            notifica = dbNotifiche.removeNotifica(user.getId(), 2);
        } catch (SQLException ex) {
            Logger.getLogger(ListaRicetteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //aggiungo le ricette alla request
        request.setAttribute("LISTA_RICETTE", ricette);
        request.getSession().setAttribute("notifica", notifica);
        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("listaRicette.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
