package it.unitn.disi.wp.progwebone.filters;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Classe Filter per gestire la sessione
 *
 * @author ProgWebOne
 *
 */
public class ValidationFilter implements Filter {

    private List<String> excludedUrls;

    /**
     * Viene analizzata la pagina e la sessione tramite la request, in caso di
     * errore si è reindirizzati sulla pagina di errore.
     * Se la pagina analizzata è inserita negli ignore sarà ignorata.
     * 
     *
     * @param req
     * @param res
     * @param chain
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String loginURI = request.getContextPath() + "/index.jsp";

        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = request.getRequestURI().equals(loginURI);

        String path = ((HttpServletRequest) req).getServletPath();

        boolean inList = excludedUrls.contains(path);

        String errorRequestURI = (String) request.getAttribute(RequestDispatcher.ERROR_REQUEST_URI);

        if (errorRequestURI != null) {
            loginURI = request.getContextPath() + "/GeneralErrorPage.html";
        }

        if (path.endsWith(".css")) {
            chain.doFilter(request, response);
            return;
        }

        if (loggedIn || loginRequest || inList) { // controllo che sia loggato e che la servlet sia nelle esclusioni
            chain.doFilter(request, response);
        }

        if (!response.isCommitted()) {
            response.sendRedirect(loginURI);
        }
    }

    /**
     * Inizializza le pagine da aggiungere all'ignore. Le pagine vengono
     * prelevate dal web.xml
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String excludePattern = filterConfig.getInitParameter("IgnoreServlet");
        excludedUrls = Arrays.asList(excludePattern.split(","));

    }

    @Override
    public void destroy() {

    }

}
