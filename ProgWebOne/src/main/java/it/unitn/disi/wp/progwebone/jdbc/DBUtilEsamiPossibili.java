package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.EsamePossibile;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.EsamePossibile}
 *
 * @author ProgWebOne
 */
public class DBUtilEsamiPossibili extends DBUtil {

    /**
     * Funzione che preleva i tipi di esami possibili dal database
     * @return Lista esami possibili
     * @throws SQLException
     */
    public List<EsamePossibile> getEsamiPossibili() throws SQLException {
        connetti();

        List<EsamePossibile> esami;
        esami = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM esami_possibili ORDER BY esame")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {

                    EsamePossibile esame = new EsamePossibile();

                    esame.setId_esame(rs.getInt("id_esame"));
                    esame.setEsame(rs.getString("esame"));

                    esami.add(esame);

                }

                shutdown();
            }
        }

        return esami;
    }

}
