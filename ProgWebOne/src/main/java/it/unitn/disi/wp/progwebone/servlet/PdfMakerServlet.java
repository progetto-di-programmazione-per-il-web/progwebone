package it.unitn.disi.wp.progwebone.servlet;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.utils.PDStreamUtils;
import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet per creare un pdf di tutti i ticket pagati da un determinato paziente
 *
 * @author ProgWebOne
 */
public class PdfMakerServlet extends HttpServlet {

    /**
     * Crea un pdf di tutti i ticket pagati da un determinato paziente
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pdfFolder = "pdfFolder";
        pdfFolder = getServletContext().getRealPath(pdfFolder);

        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();

        List<Visita> ListaVisite = null;
        List<Esame> ListaEsame = null;

        try {
            ListaVisite = dbVisite.getVisite(user.getId());
            ListaEsame = dbEsami.getEsami(user.getId());
        } catch (SQLException ex) {
            Logger.getLogger(PdfMakerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);

            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                PDStreamUtils.write(
                        contents,
                        "Lista dei Ticket Pagati",
                        PDType1Font.HELVETICA_BOLD,
                        26,
                        80,
                        700,
                        Color.BLUE);
                PDStreamUtils.write(
                        contents,
                        "Le seguenti tabelle rappresentano tutti i ticket pagati da: \"" + user.getNome() + " " + user.getCognome() + "\".",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        675,
                        Color.BLACK);

                PDStreamUtils.write(
                        contents,
                        "Esami:",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        650,
                        Color.BLACK);

                float margin = 80;
                float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
                float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

                boolean drawContent = true;
                float yStart = yStartNewPage;
                float bottomMargin = 70;
                float yPosition = 630;

                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
                Row<PDPage> header = table.createRow(ListaEsame.size());
                header.createCell(20, "Esame");
                header.createCell(20, "Esito");
                header.createCell(20, "Importo pagato");
                header.createCell(20, "Data");
                table.addHeaderRow(header);

                for (Esame esamitmp : ListaEsame) {

                    if (esamitmp.getImporto_pagato() > 0) {
                        Row<PDPage> row = table.createRow(4);
                        row.createCell(esamitmp.getEsame());
                        row.createCell(esamitmp.getEsito());
                        row.createCell(esamitmp.getPagato());
                        row.createCell(String.valueOf(esamitmp.getTimestamp()).substring(0, 19));

                    }
                }

                yStart = table.draw() - 50;

                PDStreamUtils.write(
                        contents,
                        "Visite:",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        yStart,
                        Color.BLACK);

                yStart = yStart - 20;

                BaseTable table2 = new BaseTable(yStart, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
                Row<PDPage> header2 = table2.createRow(ListaVisite.size());
                header2.createCell(20, "Visita");
                header2.createCell(20, "Esame prescritto");
                header2.createCell(20, "importo pagato");
                header2.createCell(20, "Data");
                table2.addHeaderRow(header2);

                for (Visita visitatmp : ListaVisite) {

                    if (visitatmp.getImporto_pagato() > 0) {
                        Row<PDPage> row = table2.createRow(4);
                        row.createCell(visitatmp.getVisita());
                        row.createCell(visitatmp.getEsame_prescritto());
                        row.createCell(visitatmp.getPagato());
                        row.createCell(String.valueOf(visitatmp.getTimestamp()).substring(0, 19));

                    }
                }

                table2.draw();

            }

            doc.save(new File(pdfFolder, "user-" + user.getNome() + "-" + Calendar.getInstance().getTimeInMillis() + ".pdf"));

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=Visite.pdf");
            doc.save(response.getOutputStream());
        }
    }

}
