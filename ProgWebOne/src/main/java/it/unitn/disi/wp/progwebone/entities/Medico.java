package it.unitn.disi.wp.progwebone.entities;

/**
 * Rappresenta i dati di un medico
 *
 * @author ProgWebOne
 *
 */
public class Medico {

    private int id_medico;
    private String nome;
    private String luogo_di_residenza;
    private String cognome;
    private String e_mail;
    private String provincia;
    private String password;

     /**
     * Dati:
     *
     * @param id_medico rappresenta id del medico
     * @param nome rappresenta nome del medico 
     * @param luogo_di_residenza rappresenta luogo di residenza
     * @param cognome rappresenta il cognome
     * @param e_mail rappresenta l'email
     * @param provincia rappresenta la provincia di appartenenza
     * @param password rappresenta la password
     */
    
    
    /**
     * 
     * Costruttore vuoto
     */
    
    public Medico() {

    }

    /** Costruttore per inizializzare l'user tipo medico
     *
     * @param id_medico
     * @param nome
     * @param luogo_di_residenza
     * @param cognome
     * @param e_mail
     * @param provincia
     * @param password
     */
    public Medico(int id_medico, String nome, String luogo_di_residenza, String cognome, String e_mail, String provincia, String password) {
        this.id_medico = id_medico;
        this.nome = nome;
        this.luogo_di_residenza = luogo_di_residenza;
        this.cognome = cognome;
        this.e_mail = e_mail;
        this.provincia = provincia;
        this.password = password;
    }

   /**
     * Metodo getter
     *
     * @return id_medico
     */
    public int getId_medico() {
        return id_medico;
    }

     /**
     * Metodo setter
     *
     * @param id_medico
     */
    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    /**
     * Metodo getter
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

     /**
     * Metodo setter
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo getter
     *
     * @return luogo_di_residenza
     */
    public String getLuogo_di_residenza() {
        return luogo_di_residenza;
    }

    /**
     * Metodo setter
     *
     * @param luogo_di_residenza
     */
    public void setLuogo_di_residenza(String luogo_di_residenza) {
        this.luogo_di_residenza = luogo_di_residenza;
    }

   /**
     * Metodo getter
     *
     * @return cognome
     */
    public String getCognome() {
        return cognome;
    }

     /**
     * Metodo setter
     *
     * @param cognome
     */
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    /**
     * Metodo getter
     *
     * @return e_mail
     */
    public String getE_mail() {
        return e_mail;
    }

     /**
     * Metodo setter
     *
     * @param e_mail
     */
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    /**
     * Metodo getter
     *
     * @return provincia
     */
    public String getProvincia() {
        return provincia;
    }

     /**
     * Metodo setter
     *
     * @param provincia
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

   /**
     * Metodo getter
     *
     * @return eseguito
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo setter
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 
     * 
     * 
     * @return il medico in formato string
     */
    
    
    @Override
    public String toString() {
        return "Medico{" + "id_medico=" + id_medico + ", nome=" + nome + ", luogo_di_residenza=" + luogo_di_residenza + ", cognome=" + cognome + ", e_mail=" + e_mail + ", provincia=" + provincia + ", password=" + password + '}';
    }

}
