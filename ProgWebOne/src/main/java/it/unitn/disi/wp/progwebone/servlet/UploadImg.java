package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 * Servlet per caricare una nuova foto profilo
 * per uno specifico paziente
 *
 * @author ProgWebOne
 */
@MultipartConfig
public class UploadImg extends HttpServlet {
/**
 * Carica una nuova immagine di profilo 
 * @param request
 * @param response
 * @throws ServletException
 * @throws IOException 
 */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String avatarsFolder = getServletContext().getInitParameter("avatarFolder");

        if (avatarsFolder == null) {
            throw new ServletException("Avatars folder not configured");
        }

        Paziente user = (Paziente) request.getSession().getAttribute("user");
        avatarsFolder = getServletContext().getRealPath(avatarsFolder)+"/"+user.getId();
 
 
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        Part filePart = request.getPart("avatar");
        
        if ((filePart != null) && (filePart.getSize() > 0)) {
            String filename = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();//MSIE  fix
            try (InputStream fileContent = filePart.getInputStream()) {

                
   
                File file = new File(avatarsFolder, filename);

                Files.copy(fileContent, file.toPath());

                DBUtilPaziente dbPaziente = new DBUtilPaziente();

                dbPaziente.UpdateImg(user, filename);

                request.getSession().removeAttribute("user");

                user.setAvatar_path(filename);

                request.getSession().setAttribute("user", user);

            } catch (FileAlreadyExistsException ex) {
                getServletContext().log("File \"" + filename + "\" already exists on the server");
            } catch (SQLException ex) {
                Logger.getLogger(UploadImg.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "landingPaziente.jsp"));
        }

    }

}
