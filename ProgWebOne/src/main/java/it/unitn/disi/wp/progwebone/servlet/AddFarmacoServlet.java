package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import it.unitn.disi.wp.progwebone.utilityFunction.sendEmail;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *  Servlet che gestisce l'aggiunta di farmaci, l'invio di mail e update di notifiche di tipo farmaco.
 * 
 * @author ProgWebOne
 */
public class AddFarmacoServlet extends HttpServlet {

    /**
     * 
     * Gestisce l'aggiunta di farmaci, l'invio di mail e update di notifiche di tipo farmaco.
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
    */
    
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        Medico user = (Medico) request.getSession().getAttribute("user");

        int id_paziente = Integer.valueOf(request.getParameter("id_paziente"));

        String farmaco = request.getParameter("Farmaco");
        Paziente paziente = new Paziente();
        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilRicette dbRicette = new DBUtilRicette();

        try {
            paziente = dbPaziente.getPazienteById(id_paziente);
        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (paziente.getMedicoDiBase() == user.getId_medico()) {
            try {
                dbRicette.InserisciFarmaco(id_paziente, user.getId_medico(), farmaco);
                dbNotifiche.updateNotifiche(id_paziente, 0, 0, 1);

                sendEmail.send(paziente.getEmail(), "Ti è stato prescritto un nuovo farmaco");

            } catch (SQLException ex) {
                Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "DatiPaziente?id=" + id_paziente));
        } else {
            response.sendError(500);
            return;
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
