package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.EsamePossibile;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsamiPossibili;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che gestisce i dati di un paziente specifico per farli vedere ad un determinato medico 
 * @author ProgWebOne
 */



public class DatiPazienteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            datiPaziente(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            datiPaziente(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }
/**
 * Inserisce nella request tutti i dati di un determinato paziente
 * @param request
 * @param response
 * @throws ServletException 
 */
    private void datiPaziente(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Medico medico = new Medico();
        ServizioSanitarioProvinciale servizio = new ServizioSanitarioProvinciale();
        if (LoginServlet.tipoM_P == 0) {
            medico = (Medico) request.getSession(false).getAttribute("user");
        } else {
            servizio = (ServizioSanitarioProvinciale) request.getSession(false).getAttribute("user");
        }

        Integer idPaziente = null;

        try {
            idPaziente = Integer.valueOf(request.getParameter("id"));
        } catch (RuntimeException ex) {
            throw new ServletException(ex);
        }

        Paziente paziente = null;
        List<Visita> visite = new ArrayList<>();
        List<Ricetta> ricette = new ArrayList<>();
        List<Esame> esami = new ArrayList<>();
        List<EsamePossibile> esami_possibili = new ArrayList<>();

        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilEsamiPossibili dbEsamiPossibili = new DBUtilEsamiPossibili();
        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();
        DBUtilRicette dbRicette = new DBUtilRicette();

        try {
            paziente = dbPaziente.getPazienteById(idPaziente);
            visite = dbVisite.getVisite(idPaziente);
            ricette = dbRicette.getRicette(idPaziente);
            esami = dbEsami.getEsami(idPaziente);
            esami_possibili = dbEsamiPossibili.getEsamiPossibili();

            request.setAttribute("paziente", paziente);
            request.setAttribute("LISTA_VISITE", visite);
            request.setAttribute("LISTA_RICETTE", ricette);
            request.setAttribute("LISTA_ESAMI", esami);
            request.setAttribute("ESAMI_POSSIBILI", esami_possibili);
        } catch (SQLException ex) {
            Logger.getLogger(DatiPazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //mando alla jsp (view)
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        String redirect;
        if (LoginServlet.tipoM_P == 0 && medico.getId_medico() == paziente.getMedicoDiBase()) {
            redirect = "datiPaziente.html?id=" + idPaziente;
        } else {
            if (servizio.getId() == paziente.getId_servizio()) {
                redirect = "datiPazienteServizio.html?id=" + idPaziente;
            } else {
                response.sendError(500);
                return;
            }
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(redirect);
        dispatcher.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
