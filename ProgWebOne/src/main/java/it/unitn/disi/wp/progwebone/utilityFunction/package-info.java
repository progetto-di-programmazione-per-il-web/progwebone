/**
 * Provvede due classi utility per inviare e-mail in automatico e creare file in formato xls
 * <p>
 * Contiene due classi:
 * <p>
 * {@link it.unitn.disi.wp.progwebone.utilityFunction.sendEmail},
 * {@link it.unitn.disi.wp.progwebone.utilityFunction.XlsMaker}
 *
 * @author ProgWebOne
 * @since 1.0
 * @see it.unitn.disi.wp.progwebone.utilityFunction
 */
package it.unitn.disi.wp.progwebone.utilityFunction;
