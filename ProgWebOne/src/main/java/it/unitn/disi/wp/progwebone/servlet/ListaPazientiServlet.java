package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede la lista dei pazienti di uno specifico medico
 * @author ProgWebOne
 */
public class ListaPazientiServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listPazienti(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listPazienti(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }
/**
 * Richiede la lista dei pazienti di uno specifico medico
 * @param request
 * @param response
 * @throws Exception 
 */
    private void listPazienti(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //modificare e mettere visite
        //recupero i pazienti dal db
        List<Paziente> pazienti;
        DBUtilPaziente dbPaziente = new DBUtilPaziente();

        Medico user = (Medico) request.getSession().getAttribute("user");

        pazienti = dbPaziente.getPazienti(user.getId_medico());

        //aggiungo i pazienti alla request
        request.setAttribute("LISTA_PAZIENTI", pazienti);

        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("listaPazienti.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
