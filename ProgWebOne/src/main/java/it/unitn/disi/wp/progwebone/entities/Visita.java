package it.unitn.disi.wp.progwebone.entities;

import java.sql.Timestamp;

/**
 * Rappresenta i dati di una visita
 *
 * @author ProgWebOne
 *
 */
public class Visita {

    private int id_visita;
    private int id_paziente;
    private int id_medico;
    private String visita;
    private String esame_prescritto;
    private Timestamp timestamp;
    private String pagato;
    private int importo_pagato;

    /**
     * Dati:
     *
     * @param id_visita rappresenta id della visita
     * @param id_paziente rappresenta id del paziente
     * @param id_medico rappresenta id del medico
     * @param visita rappresenta nome della visita
     * @param esame_prescritto rappresenta il tipo di esame prescritto
     * @param timestamp rappresenta data di prescrizione
     * @param pagato rappresenta importo pagato in formato stringa
     * @param importo_pagato rappresenta importo pagato in formato int
     */
    /**
     * Metodo getter
     *
     * @return importo_pagato
     */
    public int getImporto_pagato() {
        return importo_pagato;
    }

    /**
     * Metodo setter
     *
     * @param importo_pagato
     */
    public void setImporto_pagato(int importo_pagato) {
        this.importo_pagato = importo_pagato;
    }

    /**
     * Metodo getter
     *
     * @return pagato
     */
    public String getPagato() {
        return pagato;
    }

    /**
     * Metodo setter
     *
     * @param pagato
     */
    public void setPagato(String pagato) {
        this.pagato = pagato;
    }

    /**
     * Metodo getter
     *
     * @return timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Metodo setter
     *
     * @param timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Costruttore vuoto
     */
    public Visita() {

    }

    /**
     *
     * @param id_visita
     * @param id_paziente
     * @param id_medico
     * @param visita
     * @param esame_prescritto
     */
    public Visita(int id_visita, int id_paziente, int id_medico, String visita, String esame_prescritto) {
        this.id_visita = id_visita;
        this.id_paziente = id_paziente;
        this.id_medico = id_medico;
        this.visita = visita;
        this.esame_prescritto = esame_prescritto;
    }

    /**
     * Metodo getter
     *
     * @return id_visita
     */
    public int getId_visita() {
        return id_visita;
    }

    /**
     * Metodo setter
     *
     * @param id_visita
     */
    public void setId_visita(int id_visita) {
        this.id_visita = id_visita;
    }

    /**
     * Metodo getter
     *
     * @return id_paziente
     */
    public int getId_paziente() {
        return id_paziente;
    }

    /**
     * Metodo setter
     *
     * @param id_paziente
     */
    public void setId_paziente(int id_paziente) {
        this.id_paziente = id_paziente;
    }

    /**
     * Metodo getter
     *
     * @return id_medico
     */
    public int getId_medico() {
        return id_medico;
    }

    /**
     * Metodo setter
     *
     * @param id_medico
     */
    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    /**
     * Metodo getter
     *
     * @return visita
     */
    public String getVisita() {
        return visita;
    }

    /**
     * Metodo setter
     *
     * @param visita
     */
    public void setVisita(String visita) {
        this.visita = visita;
    }

    /**
     * Metodo getter
     *
     * @return esame_prescritto
     */
    public String getEsame_prescritto() {
        return esame_prescritto;
    }

    /**
     * Metodo setter
     *
     * @param esame_prescritto
     */
    public void setEsame_prescritto(String esame_prescritto) {
        this.esame_prescritto = esame_prescritto;
    }

}
