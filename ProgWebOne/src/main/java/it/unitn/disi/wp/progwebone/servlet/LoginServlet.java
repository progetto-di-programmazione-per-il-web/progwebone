package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.jdbc.DBUtil;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilMedico;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.utilityFunction.Sha256;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che gestisce il login, crea cookies per il pulsante ricordami e riconosce il tipo di login da effettuare
 *
 * @author ProgWebOne
 */
public class LoginServlet extends HttpServlet {

    static int tipoM_P;

    /**
     * Gestisce il login e reindirizza sulla landing corretta
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        DBUtil util = new DBUtil();

        String email = request.getParameter("username");
        String password = request.getParameter("password");
        String tipo = request.getParameter("radioLogin");
        boolean rememberBool = false;

        if (email == null) {

            Cookie[] cookies = request.getCookies();
            String userName = "", token = "", CrememberVal = "";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("cookuser")) {
                        userName = cookie.getValue();
                        email = cookie.getValue();
                        password = "";
                    }
                    if (cookie.getName().equals("cookpass")) {
                        token = cookie.getValue();
                    }
                    if (cookie.getName().equals("cookrem")) {
                        CrememberVal = cookie.getValue();
                    }
                    if (cookie.getName().equals("cookTipo")) {
                        tipo = cookie.getValue();
                    }
                }
            }
            try {
                rememberBool = util.checkRememberMe(userName, token);
            } catch (SQLException ex) {
                Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        String redirect = "index.jsp"; //di default se non fa nulla (provvisorio fino a sistema notifiche errore)

        //Controllo il Tipo di login    
        if ("a".equals(tipo)) {
            request.setAttribute("tipo", 1); // paziente 1
            tipoM_P = 1;
            redirect = "LoadNotificheServlet";

        }

        if ("b".equals(tipo)) {

            request.setAttribute("tipo", 0); // medico 0
            tipoM_P = 0;
            redirect = "LandingMedico";
        }

        if ("c".equals(tipo)) {

            request.setAttribute("tipo", 2); // provincia 2
            tipoM_P = 2;
            redirect = "LandingServizio";

        }

        try {
            if (util.Validation(email, password, tipoM_P) || rememberBool) { // validation con pass e mail 

                request.setAttribute("code", 1);

                if (tipoM_P == 2) {

                    ServizioSanitarioProvinciale user;
                    DBUtilServizioSanitarioProvinciale dbServizioSanitarioProvinciale = new DBUtilServizioSanitarioProvinciale();
                    user = dbServizioSanitarioProvinciale.getServizioSanitarioProvinciale(email);

                    request.getSession().setAttribute("user", user);

                }

                if (tipoM_P == 1) {

                    Paziente user;
                    DBUtilPaziente dbPaziente = new DBUtilPaziente();
                    DBUtilMedico dbMedico = new DBUtilMedico();
                    user = dbPaziente.getPaziente(email);

                    for (Medico medTmp : dbMedico.getMedicoByProvincia(user.getProvincia())) {
                        if (medTmp.getId_medico() == user.getMedicoDiBase()) {
                            request.getSession().setAttribute("medico", medTmp);
                        }
                    }

                    request.getSession().setAttribute("user", user);
                    request.getSession().setAttribute("lista_medici", dbMedico.getMedicoByProvincia(user.getProvincia()));

                }

                if (tipoM_P == 0) {

                    Medico user;
                    DBUtilMedico dbMedico = new DBUtilMedico();
                    user = dbMedico.getMedico(email);

                    request.getSession().setAttribute("user", user);

                }

                if (request.getParameter("remember") != null) {

                    Random rand = new Random();

                    int token = rand.nextInt(16384);
                    Sha256 enc = new Sha256();

                    String remember = request.getParameter("remember");
                    Cookie cUserName = new Cookie("cookuser", email.trim());
                    Cookie cPassword = new Cookie("cookpass", enc.encrypter("" + token));
                    Cookie cRemember = new Cookie("cookrem", remember.trim());
                    Cookie cTipo = new Cookie("cookTipo", tipo);
                    cUserName.setMaxAge(60 * 60 * 24 * 365 * 10);// 10 anni
                    cPassword.setMaxAge(60 * 60 * 24 * 365 * 10);
                    cRemember.setMaxAge(60 * 60 * 24 * 365 * 10);
                    cTipo.setMaxAge(60 * 60 * 24 * 15);
                    response.addCookie(cUserName);
                    response.addCookie(cPassword);
                    response.addCookie(cRemember);
                    response.addCookie(cTipo);

                    util.insertRememberMe(email, enc.encrypter("" + token));

                }

            } else {
                Cookie cUserName = new Cookie("cookuser", null);
                Cookie cPassword = new Cookie("cookpass", null);
                Cookie cRemember = new Cookie("cookrem", null);
                Cookie cTipo = new Cookie("cookTipo", null);
                cUserName.setMaxAge(0);
                cPassword.setMaxAge(0);
                cRemember.setMaxAge(0);
                cTipo.setMaxAge(0);
                response.addCookie(cUserName);
                response.addCookie(cPassword);
                response.addCookie(cRemember);
                response.addCookie(cTipo);

                redirect = "index.jsp";

                request.getSession().setAttribute("erroreLogin", 1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //rimando in base al nome
        response.sendRedirect(response.encodeRedirectURL(redirect));
    }

}
