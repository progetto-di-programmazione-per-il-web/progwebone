package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet per pagare un determinato esame
 *
 * @author ProgWebOne
 */
public class PagaEsameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            pagaEsame(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            pagaEsame(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }
/**
 * Paga uno specifico esame
 * @param request
 * @param response
 * @throws Exception 
 */
    private void pagaEsame(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Paziente user = (Paziente) request.getSession().getAttribute("user");
        Integer id_esame = null;

        try {
            id_esame = Integer.valueOf(request.getParameter("id"));
        } catch (RuntimeException ex) {
            //TODO: Handle the exception
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        DBUtilEsami dbEsami = new DBUtilEsami();
        Esame esame = null;
        esame = new Esame();

        try {
            esame = dbEsami.getEsameById(id_esame);

        } catch (SQLException ex) {
            Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (esame.getId_paziente() == user.getId() && esame.getImporto_pagato() == 0) {
            try {
                dbEsami.pagaEsame(id_esame);

            } catch (SQLException ex) {
                Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "ListaEsami"));
        }else{
            response.sendError(500);
            return;
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
