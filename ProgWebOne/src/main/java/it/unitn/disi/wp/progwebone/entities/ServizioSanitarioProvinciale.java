package it.unitn.disi.wp.progwebone.entities;

/**
 * Rappresenta i dati del Servizio Sanitario Provinciale
 *
 * @author ProgWebOne
 *
 */
public class ServizioSanitarioProvinciale {

    private String nome;
    private String provincia;
    private String e_mail;
    private String password;
    private int id;

    /**
     * Dati:
     *
     * @param nome rappresenta nome della provincia in formato intero
     * @param provincia rappresenta nome della provincia in formato ridotto
     * @param e_mail rappresenta email
     * @param password rappresenta la password
     * @param id rappresenta id della provincia
     */
    /**
     * Costruttore vuoto
     */
    public ServizioSanitarioProvinciale() {

    }

    /**
     * Metodo getter
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Metodo setter
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo getter
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo setter
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo getter
     *
     * @return provincia
     */
    public String getProvincia() {
        return provincia;
    }

    /**
     * Metodo setter
     *
     * @param provincia
     */
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /**
     * Metodo getter
     *
     * @return e_mail
     */
    public String getE_mail() {
        return e_mail;
    }

    /**
     * Metodo setter
     *
     * @param e_mail
     */
    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    /**
     * Metodo getter
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo setter
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
