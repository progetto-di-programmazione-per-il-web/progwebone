package it.unitn.disi.wp.progwebone.entities;

import java.sql.Timestamp;

/**
 * Rappresenta i dati di una ricetta
 *
 * @author ProgWebOne
 *
 */
public class Ricetta {

    private int id_ricetta;
    private int id_paziente;
    private int id_medico;
    private String farmaco;
    private Timestamp timestamp;

    /**
     * Dati:
     *
     * @param id_ricetta rappresenta id della ricetta
     * @param id_paziente rappresenta id del paziente
     * @param id_medico rappresenta id del medico
     * @param farmaco rappresenta nome del farmaco
     * @param timestamp rappresenta la data della prescizione
     */
    /**
     * Metodo getter
     *
     * @return timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Metodo getter
     *
     * @return TimeStamp in formato string eliminando i millesimi di secondo e
     * mantenendo solo la data
     */
    public String getTimetoString() {

        return String.valueOf((timestamp)).substring(0, 19);
    }

    /**
     * Metodo setter
     *
     * @param timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Costruttore vuoto
     */
    public Ricetta() {

    }

    /**
     * Metodo getter
     *
     * @return id_ricetta
     */
    public int getId_ricetta() {
        return id_ricetta;
    }

    /**
     * Metodo setter
     *
     * @param id_ricetta
     */
    public void setId_ricetta(int id_ricetta) {
        this.id_ricetta = id_ricetta;
    }

    /**
     *
     * Metodo getter
     *
     * @return id_paziente
     */
    public int getId_paziente() {
        return id_paziente;
    }

    /**
     * Metodo setter
     *
     * @param id_paziente
     */
    public void setId_paziente(int id_paziente) {
        this.id_paziente = id_paziente;
    }

    /**
     * Metodo getter
     *
     * @return id_medico
     */
    public int getId_medico() {
        return id_medico;
    }

    /**
     * Metodo setter
     *
     * @param id_medico
     */
    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    /**
     * Metodo getter
     *
     * @return farmaco
     */
    public String getFarmaco() {
        return farmaco;
    }

    /**
     * Metodo setter
     *
     * @param farmaco
     */
    public void setFarmaco(String farmaco) {
        this.farmaco = farmaco;
    }

}
