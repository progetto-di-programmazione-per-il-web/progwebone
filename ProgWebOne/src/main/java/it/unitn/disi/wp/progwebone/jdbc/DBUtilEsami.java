package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Esame;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Esame}
 *
 * @author ProgWebOne
 */
public class DBUtilEsami extends DBUtil {

    /**
     * Prende in input id di un paziente e ritorna una lista dei suoi
     * esami
     *
     *
     *
     * @param id_paziente
     * @return Lista Esami
     * @throws SQLException
     */
    public List<Esame> getEsami(int id_paziente) throws SQLException {

        connetti();

        List<Esame> esami;
        esami = new ArrayList<>();

        try ( PreparedStatement stm = conn.prepareStatement("SELECT * FROM esami where id_paziente = " + id_paziente)) {
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Esame esameTmp = new Esame();

                    esameTmp.setEsame(rs.getString("esame"));
                    esameTmp.setId_medico(rs.getInt("id_medico"));
                    esameTmp.setId_paziente(rs.getInt("id_paziente"));
                    esameTmp.setId_esame(rs.getInt("id_esame"));
                    esameTmp.setEsito(rs.getString("esito"));
                    esameTmp.setTimestamp(rs.getTimestamp("data_ora"));
                    esameTmp.setPagato(rs.getString("pagato"));
                    esameTmp.setImporto_pagato(rs.getInt("importo_pagato"));
                    esameTmp.setEseguito(rs.getInt("eseguito"));

                    esami.add(esameTmp);
                }

                shutdown();
            }
        }

        return esami;

    }

    /**
     * Funzione per inserire un nuovo esame nel database dato:
     *
     * @param ID_PAZIENTE
     * @param ID_MEDICO
     * @param ESAME
     * @throws SQLException
     */
    public void InserisciEsame(int ID_PAZIENTE, int ID_MEDICO, String ESAME) throws SQLException {

        connetti();

        String sql = "insert into esami ( ID_PAZIENTE, ID_MEDICO, ESAME, ESITO, DATA_ORA, PAGATO) VALUES (?,?,?,?,?,?)";
        // create the sql insert preparedstatement
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, ID_PAZIENTE);
        preparedStmt.setInt(2, ID_MEDICO);
        preparedStmt.setString(3, ESAME);
        preparedStmt.setString(4, "Non eseguito");
        preparedStmt.setTimestamp(5, Timestamp.from(Instant.now()));
        preparedStmt.setString(6, "Non pagato");

        preparedStmt.executeUpdate();

        shutdown();
    }

    /**
     * Funzione per pagare un esame dato id dell'esame
     *
     * @param id_esame
     * @throws SQLException
     */
    public void pagaEsame(int id_esame) throws SQLException {

        try {

            connetti();

            PreparedStatement ps = conn.prepareStatement("UPDATE ESAMI SET PAGATO = ?, IMPORTO_PAGATO = ? WHERE ID_ESAME = ?");

            ps.setString(1, "Pagato - 11 €");
            ps.setInt(2, 11);
            ps.setInt(3, id_esame);

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            // log the exception
            throw se;
        }

        shutdown();

    }

    /**
     * Funzione per eseguire un esame dato id dell'esame
     *
     * @param id_esame
     * @param esito
     * @throws SQLException
     */
    public void eseguiEsame(int id_esame, String esito) throws SQLException {

        try {

            connetti();

            PreparedStatement ps = conn.prepareStatement("UPDATE ESAMI SET ESITO = ?, ESEGUITO = ?, DATA_ORA = ? WHERE ID_ESAME =" + id_esame);

            ps.setString(1, esito);
            ps.setInt(2, 1);
            ps.setTimestamp(3, Timestamp.from(Instant.now())); 

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            // log the exception
            throw se;
        }

        shutdown();

    }

    /**
     * Prende in input id di un medico e ritorna una lista dei suoi
     * esami
     * @param id_medico
     * @return
     * @throws SQLException
     */
    public List<Esame> getEsamiByMedico(int id_medico) throws SQLException {
        connetti();

        List<Esame> esami;
        esami = new ArrayList<>();

        try ( PreparedStatement stm = conn.prepareStatement("SELECT * FROM esami where id_medico = " + id_medico)) {
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Esame esameTmp = new Esame();

                    esameTmp.setEsame(rs.getString("esame"));
                    esameTmp.setId_medico(rs.getInt("id_medico"));
                    esameTmp.setId_paziente(rs.getInt("id_paziente"));
                    esameTmp.setId_esame(rs.getInt("id_esame"));
                    esameTmp.setEsito(rs.getString("esito"));
                    esameTmp.setTimestamp(rs.getTimestamp("data_ora"));
                    esameTmp.setPagato(rs.getString("pagato"));
                    esameTmp.setImporto_pagato(rs.getInt("importo_pagato"));
                    esameTmp.setEseguito(rs.getInt("eseguito"));

                    esami.add(esameTmp);
                }

                shutdown();
            }
        }

        return esami;

    }
    /**Ritorna un esame dato il suo id
     * 
     * @param id_esame
     * @return esame
     * @throws SQLException 
     */
    public Esame getEsameById(int id_esame) throws SQLException {

        connetti();

        Esame esame = null;
        esame = new Esame();

        try ( PreparedStatement stm = conn.prepareStatement("SELECT * FROM esami where id_esame = " + id_esame)) {
            try ( ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    esame.setEsame(rs.getString("esame"));
                    esame.setId_medico(rs.getInt("id_medico"));
                    esame.setId_paziente(rs.getInt("id_paziente"));
                    esame.setId_esame(rs.getInt("id_esame"));
                    esame.setEsito(rs.getString("esito"));
                    esame.setTimestamp(rs.getTimestamp("data_ora"));
                    esame.setPagato(rs.getString("pagato"));
                    esame.setImporto_pagato(rs.getInt("importo_pagato"));
                    esame.setEseguito(rs.getInt("eseguito"));
                }
                shutdown();
            }
        }
        return esame;
    }

}
