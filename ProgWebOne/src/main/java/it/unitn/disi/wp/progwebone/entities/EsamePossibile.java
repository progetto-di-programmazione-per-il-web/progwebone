package it.unitn.disi.wp.progwebone.entities;

/**
 * Rappresenta i dati di possibili esami prescrivibili
 *
 * @author ProgWebOne
 */
public class EsamePossibile {

    private int id_esame;
    private String esame;

    /**
     * Dati:
     *
     * @param id_esame rappresenta id univoco dell'esame
     * @param esame rappresenta una descrizione dell'esame
     */

    /**
     * costruttore vuoto
     */
    public EsamePossibile() {

    }

    /**
     * Metodo getter
     *
     * @return eseguito
     */
    public int getId_esame() {
        return id_esame;
    }

    /**
     * Metodo setter
     *
     * @param id_esame
     */
    public void setId_esame(int id_esame) {
        this.id_esame = id_esame;
    }

    /**
     * Metodo getter
     *
     * @return esame
     */
    public String getEsame() {
        return esame;
    }

    /**
     * Metodo setter
     *
     * @param esame
     */
    public void setEsame(String esame) {
        this.esame = esame;
    }

}
