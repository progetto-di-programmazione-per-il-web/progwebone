/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilMedico;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.utilityFunction.Sha256;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che modifica i dati dell'account di un paziente
 *
 * @author ProgWebOne
 */
@WebServlet(name = "ModificaDatiServlet", urlPatterns = {"/ModificaDatiServlet"})
public class ModificaDatiServlet extends HttpServlet {

    /**
     * Modifica i dati dell'account di un paziente e lo aggiorna nel database
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        int medicoId = Integer.parseInt(request.getParameter("medicoDiBaseMod"));
        String email = request.getParameter("Email");
        String password = request.getParameter("Password");
        Paziente tmpPaziente = (Paziente) request.getSession().getAttribute("user");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        if(!password.equals("nopass")){
            Sha256 enc = new Sha256();
            password = enc.encrypter(password);
        }else{
            password = tmpPaziente.getPassword();
        }
        
        
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        
        try {
            dbPaziente.modifyPaziente(email, medicoId, tmpPaziente.getId(), password);
            
            tmpPaziente.setE_mail(email);
            tmpPaziente.setPassword(password);
            
            if (medicoId != -1) {
                tmpPaziente.setMedico_di_base(medicoId);
                DBUtilMedico db = new DBUtilMedico();
                request.getSession().setAttribute("medico", db.getMedicoByIdMedico(medicoId));
            }
            
            request.getSession().removeAttribute("user");
            request.getSession().setAttribute("user", tmpPaziente);
            
        } catch (SQLException ex) {
            Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "landingPaziente.jsp"));
        
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
