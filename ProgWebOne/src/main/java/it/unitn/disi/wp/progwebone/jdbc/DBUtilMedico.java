package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Medico;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Medico}
 *
 * @author ProgWebOne
 */
public class DBUtilMedico extends DBUtil {

    /** 
     * Restituisce un medico data un'email 
     *
     * @param email
     * @return
     * @throws SQLException
     */
    public Medico getMedico(String email) throws SQLException {
        connetti();
        Medico medico = null;
        medico = new Medico();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM medici Where E_mail = '" + email + "'")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    medico.setId_medico(rs.getInt("ID_MEDICO"));
                    medico.setE_mail(rs.getString("E_MAIL"));                 
                    medico.setNome(rs.getString("NOME"));
                    medico.setCognome(rs.getString("COGNOME"));
                    medico.setProvincia(rs.getString("PROVINCIA"));
                    medico.setLuogo_di_residenza("LUOGO_DI_RESIDENZA");

                }
                shutdown();
            }
        }
        return medico;

    }

    /**
     * Restituisce una lista di medici appartenente ad una determinata provincia
     * @param Provincia
     * @return lista di medici
     * @throws SQLException
     */
    public List<Medico> getMedicoByProvincia(String Provincia) throws SQLException {
        connetti();

        List<Medico> medici;
        medici = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM medici where provincia = '" + Provincia + "' ORDER BY id_medico")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Medico medico = new Medico();
                    medico.setId_medico(rs.getInt("id_medico"));
                    medico.setE_mail(rs.getString("e_mail"));
                    medico.setNome(rs.getString("nome"));
                    medico.setCognome(rs.getString("cognome"));
                    medico.setProvincia(rs.getString("provincia"));
                    medico.setLuogo_di_residenza("luogo_di_residenza");

                    medici.add(medico);
                }
                shutdown();
            }
        }

        return medici;
    }

    /**
     * Restituisce un medico dato il suo id
     * @param idMedico
     * @return
     * @throws SQLException
     */
    public Medico getMedicoByIdMedico(int idMedico) throws SQLException {
        connetti();

        Medico medico = new Medico();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM medici where id_medico =" + idMedico)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {

                    medico.setId_medico(rs.getInt("id_medico"));
                    medico.setE_mail(rs.getString("e_mail"));
                    medico.setNome(rs.getString("nome"));
                    medico.setCognome(rs.getString("cognome"));
                    medico.setProvincia(rs.getString("provincia"));
                    medico.setLuogo_di_residenza("luogo_di_residenza");

                }
                shutdown();
            }
        }

        return medico;
    }
}
