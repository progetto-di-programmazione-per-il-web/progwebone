
/**
 * Provvede 9 classi per gestire connessioni 
 * <p>
 * Contiene 9 classe: 
 * <p>
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtil},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilEsamiPossibili},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilMedico},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilServizioSanitarioProvinciale},
 * {@link it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite}
 * 
 * 
 * @author ProgWebOne
 * @since 1.0
 * @see it.unitn.disi.wp.progwebone.jdbc
 */
package it.unitn.disi.wp.progwebone.jdbc;