package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede la lista dei pazienti di una specifica provincia
 *
 * @author ProgWebOne
 */
public class ListaServizioSanitarioServlet extends HttpServlet {

    /**
     * Richiede la lista dei pazienti di una specifica provincia e la inserisce
     * nella request
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listPazientiByProvincia(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //modificare e mettere visite
        //recupero i pazienti dal db
        List<Paziente> pazienti = null;
        DBUtilPaziente dbPaziente = new DBUtilPaziente();

        ServizioSanitarioProvinciale user = (ServizioSanitarioProvinciale) request.getSession().getAttribute("user");

        try {
            pazienti = dbPaziente.getPazientiByProvincia(user.getProvincia());
        } catch (SQLException ex) {
            Logger.getLogger(ListaServizioSanitarioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //aggiungo i pazienti alla request
        request.setAttribute("LISTA_PAZIENTI", pazienti);

        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("landingProvincia.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        listPazientiByProvincia(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        listPazientiByProvincia(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
