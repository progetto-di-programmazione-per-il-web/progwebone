/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import it.unitn.disi.wp.progwebone.utilityFunction.XlsMaker;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Servlet per creare report in formato .xls di tutti i farmaci prescritti in un determinato giorno
 * per uno specifico paziente
 *
 * @author ProgWebOne
 */
public class xlsMakerServlet extends HttpServlet {

    /**
     * Crea report in formato .xls di tutti i farmaci prescritti in un determinato giorno
     * @param request
     * @param response
     * @throws ServletException
     */
    protected void makeXls(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        String pdfFolder = "pdfFolder";
        pdfFolder = getServletContext().getRealPath(pdfFolder);

        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }

        XlsMaker xls = new XlsMaker();

        DBUtilRicette dbRicette = new DBUtilRicette();
        String data = (String) request.getParameter("data");

        List<String> campi = new ArrayList<>();
        campi.add("id_ricetta");
        campi.add("id_paziente");
        campi.add("id_medico");
        campi.add("farmaco");
        campi.add("Data");

        List<Ricetta> records = null;
        try {
            records = dbRicette.getRicetteByDateAndProvincia(data, (ServizioSanitarioProvinciale) request.getSession().getAttribute("user"));
        } catch (SQLException ex) {
            Logger.getLogger(xlsMakerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        File file = new File(pdfFolder, "reportProvinciale.xls");
        HSSFWorkbook wb = new HSSFWorkbook();
        try {
            xls.printWorkbook(campi, records, wb, file);

            response.setContentType("text/xls");
            response.setHeader("Content-disposition", "attachment; filename=reportProvinciale_" + data + ".xls");

            try (OutputStream outputStream = response.getOutputStream()) {
                wb.write(outputStream);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        makeXls(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        makeXls(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
