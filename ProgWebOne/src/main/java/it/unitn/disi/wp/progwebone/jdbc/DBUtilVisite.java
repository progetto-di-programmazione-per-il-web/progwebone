package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Visita;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Visita}
 *
 * @author ProgWebOne
 */
public class DBUtilVisite extends DBUtil {

    /**
     * Ritorna una lista di visite appartenenti ad un determinato paziente
     * @param id_paziente
     * @return lista di visite
     * @throws SQLException
     */
    public List<Visita> getVisite(int id_paziente) throws SQLException {

        connetti();

        List<Visita> visite;
        visite = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM visite where id_paziente = " + id_paziente)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Visita visitaTmp = new Visita();

                    visitaTmp.setEsame_prescritto(rs.getString("esame_prescritto"));
                    visitaTmp.setId_medico(rs.getInt("id_medico"));
                    visitaTmp.setId_paziente(rs.getInt("id_paziente"));
                    visitaTmp.setId_visita(rs.getInt("id_visita"));
                    visitaTmp.setVisita(rs.getString("visita"));
                    visitaTmp.setTimestamp(rs.getTimestamp("data_ora"));
                    visitaTmp.setPagato(rs.getString("pagato"));
                    visitaTmp.setImporto_pagato(rs.getInt("importo_pagato"));

                    visite.add(visitaTmp);
                }

                shutdown();
            }
        }

        return visite;

    }

    /**
      * Ritorna una lista di visite prescritte da un determinato medico
     * @param id_medico
     * @return lista di visite
     * @throws SQLException
     */
    public List<Visita> getVisiteByMedico(int id_medico) throws SQLException {

        connetti();

        List<Visita> visite;
        visite = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM visite where id_medico = " + id_medico + "ORDER BY data_ora DESC")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Visita visitaTmp = new Visita();

                    visitaTmp.setEsame_prescritto(rs.getString("esame_prescritto"));
                    visitaTmp.setId_medico(rs.getInt("id_medico"));
                    visitaTmp.setId_paziente(rs.getInt("id_paziente"));
                    visitaTmp.setId_visita(rs.getInt("id_visita"));
                    visitaTmp.setVisita(rs.getString("visita"));
                    visitaTmp.setTimestamp(rs.getTimestamp("data_ora"));
                    visitaTmp.setPagato(rs.getString("pagato"));
                    visitaTmp.setImporto_pagato(rs.getInt("importo_pagato"));

                    visite.add(visitaTmp);
                }

                shutdown();
            }
        }

        return visite;

    }

    /**
     * Funzione per inserire una nuova visita nel database
     * @param ID_PAZIENTE
     * @param ID_MEDICO
     * @param VISITA
     * @param ESAME_PRESCRITTO
     * @throws SQLException
     */
    public void InserisciVisita(int ID_PAZIENTE, int ID_MEDICO, String VISITA, String ESAME_PRESCRITTO) throws SQLException {

        connetti();

        String sql = "insert into visite ( ID_PAZIENTE, ID_MEDICO, VISITA, ESAME_PRESCRITTO, DATA_ORA, PAGATO) VALUES (?,?,?,?,?,?)";
        // create the sql insert preparedstatement
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, ID_PAZIENTE);
        preparedStmt.setInt(2, ID_MEDICO);
        preparedStmt.setString(3, VISITA);
        preparedStmt.setString(4, ESAME_PRESCRITTO);
        preparedStmt.setTimestamp(5, Timestamp.from(Instant.now()));
        preparedStmt.setString(6, "Non pagata");

        preparedStmt.executeUpdate();

        shutdown();
    }

    /**Funzione per pagare una visita dato il suo id 
     *
     * @param id_visita
     * @throws SQLException
     */
    public void pagaVisita(int id_visita) throws SQLException {

        try {

            connetti();

            PreparedStatement ps = conn.prepareStatement("UPDATE VISITE SET PAGATO = ?, IMPORTO_PAGATO = ? WHERE ID_VISITA = ?");

            ps.setString(1, "Pagato - 50 €");
            ps.setInt(2, 50);
            ps.setInt(3, id_visita);

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            // log the exception
            throw se;
        }

        shutdown();

    }
    /**Ritorna una visita dato il suo id
     * 
     * @param id_visita
     * @return visita
     * @throws SQLException 
     */
    public Visita getVisitaById(int id_visita) throws SQLException {

        connetti();

        Visita visita;
        visita = new Visita();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM visite where id_visita = " + id_visita)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {

                    visita.setEsame_prescritto(rs.getString("esame_prescritto"));
                    visita.setId_medico(rs.getInt("id_medico"));
                    visita.setId_paziente(rs.getInt("id_paziente"));
                    visita.setId_visita(rs.getInt("id_visita"));
                    visita.setVisita(rs.getString("visita"));
                    visita.setTimestamp(rs.getTimestamp("data_ora"));
                    visita.setPagato(rs.getString("pagato"));
                    visita.setImporto_pagato(rs.getInt("importo_pagato"));

                }

                shutdown();
            }
        }

        return visita;

    }
}
