package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale}
 *
 * @author ProgWebOne
 */
public class DBUtilServizioSanitarioProvinciale extends DBUtil {

    /**
     * Ritorna un Servizio Sanitario Provinciale in base alla mail
     * @param email
     * @return user
     * @throws SQLException
     */
    public ServizioSanitarioProvinciale getServizioSanitarioProvinciale(String email) throws SQLException {
        connetti();
        ServizioSanitarioProvinciale user = null;
        user = new ServizioSanitarioProvinciale();

        try (PreparedStatement stm = conn.prepareStatement("select * from servizi_sanitari_provinciali where E_mail = '" + email + "'")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    user.setE_mail(rs.getString("E_mail"));
                    user.setNome(rs.getString("nome"));
                    user.setPassword(rs.getString("password"));
                    user.setProvincia(rs.getString("provincia"));
                    user.setId(rs.getInt("id"));
                }
                shutdown();
            }
        }
        return user;
    }
}
