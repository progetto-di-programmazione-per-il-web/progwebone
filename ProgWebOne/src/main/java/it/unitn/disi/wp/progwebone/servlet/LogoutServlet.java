package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.jdbc.DBUtil;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet che gestisce il logout, invalida la sessione e riporta sulla welcome
 * page
 *
 * @author ProgWebOne
 */
public class LogoutServlet extends HttpServlet {

    /**
     * Invalida la sessione e riporta alla welcome page
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession(false);
        if (session != null) {
            Paziente paziente = new Paziente();
            Medico medico = new Medico();
            ServizioSanitarioProvinciale servizio = new ServizioSanitarioProvinciale();
            if (LoginServlet.tipoM_P == 1) {
                paziente = (Paziente) session.getAttribute("user");
            } else {
                if (LoginServlet.tipoM_P == 0) {
                    medico = (Medico) session.getAttribute("user");
                } else {
                    servizio = (ServizioSanitarioProvinciale) session.getAttribute("user");
                }

            }

            if (paziente != null || medico != null || servizio != null) {   //capire il perchè
                session.setAttribute("user", null);
                session.invalidate();

                if (LoginServlet.tipoM_P == 1) {
                    paziente = null;
                } else {
                    if (LoginServlet.tipoM_P == 0) {
                        medico = null;
                    } else {
                        servizio = null;
                    }

                }
            }
        }

        DBUtil util = new DBUtil();

        Cookie[] cookies = request.getCookies();
        String userName = "", token = "", CrememberVal = "";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("cookuser")) {
                    userName = cookie.getValue();
                }
                if (cookie.getName().equals("cookpass")) {
                    token = cookie.getValue();
                }

            }
        }

        try {
            util.DeleteRememberMe(userName, token);
        } catch (SQLException ex) {
            Logger.getLogger(LogoutServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        Cookie cUserName = new Cookie("cookuser", null);
        Cookie cPassword = new Cookie("cookpass", null);
        Cookie cRemember = new Cookie("cookrem", null);
        Cookie cTipo = new Cookie("cookTipo", null);
        cUserName.setMaxAge(0);
        cPassword.setMaxAge(0);
        cRemember.setMaxAge(0);
        cTipo.setMaxAge(0);
        response.addCookie(cUserName);
        response.addCookie(cPassword);
        response.addCookie(cRemember);
        response.addCookie(cTipo);

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        if (!response.isCommitted()) {
            response.sendRedirect(response.encodeRedirectURL(contextPath + "index.jsp"));
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
