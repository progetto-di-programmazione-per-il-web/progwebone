package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Notifiche;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede la lista degli esami di uno specifico paziente
 * @author ProgWebOne
 */
public class ListaEsamiServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaEsami(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaEsami(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }
    
    
    /**
     * Richiede la lista degli esami di uno specifico paziente
     * @param request
     * @param response
     * @throws Exception 
     */

    private void listaEsami(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<Esame> esami = new ArrayList<>();

        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilEsami dbEsami = new DBUtilEsami();

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        Notifiche notifica = null;
        try {
            esami = dbEsami.getEsami(user.getId());

            notifica = dbNotifiche.removeNotifica(user.getId(), 1);

        } catch (SQLException ex) {
            Logger.getLogger(ListaEsamiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //aggiungo i pazienti alla request
        request.setAttribute("LISTA_ESAMI", esami);
        request.getSession().setAttribute("notifica", notifica);

        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("listaEsami.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
