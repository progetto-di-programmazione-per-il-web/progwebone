package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Paziente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Paziente}
 *
 * @author ProgWebOne
 */
public class DBUtilPaziente extends DBUtil {

    /**
     * Restituisce una lista di tutti i pazienti appartenenti a un determinato
     * medico
     *
     * @param id_medico
     * @return List Pazienti
     * @throws SQLException
     */
    public List<Paziente> getPazienti(int id_medico) throws SQLException {

        connetti();

        List<Paziente> pazienti;
        pazienti = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM pazienti where Medico_Di_Base = " + id_medico + " ORDER BY id_paziente")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente();
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setE_mail(rs.getString("e_mail"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setCodice_fiscale(rs.getString("codice_fiscale"));
                    paziente.setData_di_nascita(rs.getString("data_di_nascita"));
                    paziente.setLuogo_di_nascita(rs.getString("luogo_di_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setProvincia(rs.getString("provincia"));
                    paziente.setMedico_di_base(rs.getInt("Medico_Di_Base"));
                    paziente.setAvatar_path(rs.getString("avatar_path"));
                    paziente.setId_servizio(rs.getInt("id_servizio"));

                    pazienti.add(paziente);
                }
                shutdown();
            }
        }

        return pazienti;
    }

    /**
     * Restituisce un paziente data la sua mail
     *
     * @param email
     * @return Paziente
     * @throws SQLException
     */
    public Paziente getPaziente(String email) throws SQLException {

        connetti();

        Paziente paziente;
        paziente = new Paziente();
        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM pazienti Where E_mail = '" + email + "'")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setE_mail(rs.getString("e_mail"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setCodice_fiscale(rs.getString("codice_fiscale"));
                    paziente.setData_di_nascita(rs.getString("data_di_nascita"));
                    paziente.setLuogo_di_nascita(rs.getString("luogo_di_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setProvincia(rs.getString("provincia"));
                    paziente.setMedico_di_base(rs.getInt("Medico_Di_Base"));
                    paziente.setAvatar_path(rs.getString("avatar_path"));
                    paziente.setId_servizio(rs.getInt("id_servizio"));
                    paziente.setPassword(rs.getString("password"));
                }
            }
            shutdown();
        }

        return paziente;
    }

    /**
     * Restituisce un paziente dato il suo id
     *
     * @param id
     * @return id Paziente
     * @throws SQLException
     */
    public Paziente getPazienteById(int id) throws SQLException {

        connetti();

        Paziente paziente;
        paziente = new Paziente();
        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM pazienti Where id_paziente = " + id)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setE_mail(rs.getString("e_mail"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setCodice_fiscale(rs.getString("codice_fiscale"));
                    paziente.setData_di_nascita(rs.getString("data_di_nascita"));
                    paziente.setLuogo_di_nascita(rs.getString("luogo_di_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setProvincia(rs.getString("provincia"));
                    paziente.setMedico_di_base(rs.getInt("Medico_Di_Base"));
                    paziente.setAvatar_path(rs.getString("avatar_path"));
                    paziente.setId_servizio(rs.getInt("id_servizio"));
                }
            }
            shutdown();
        }

        return paziente;
    }

    /**
     * Restituisce una lista di pazienti appartenenti ad una determinata
     * provincia
     *
     * @param provincia
     * @return Lista pazienti
     * @throws SQLException
     */
    public List<Paziente> getPazientiByProvincia(String provincia) throws SQLException {

        connetti();

        List<Paziente> pazienti;
        pazienti = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM pazienti where provincia = '" + provincia + "'")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Paziente paziente = new Paziente();
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setE_mail(rs.getString("e_mail"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setCodice_fiscale(rs.getString("codice_fiscale"));
                    paziente.setData_di_nascita(rs.getString("data_di_nascita"));
                    paziente.setLuogo_di_nascita(rs.getString("luogo_di_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setProvincia(rs.getString("provincia"));
                    paziente.setMedico_di_base(rs.getInt("Medico_Di_Base"));
                    paziente.setAvatar_path(rs.getString("avatar_path"));
                    paziente.setId_servizio(rs.getInt("id_servizio"));

                    pazienti.add(paziente);
                }
                shutdown();
            }
        }

        return pazienti;
    }

    /**
     * Restituisce un paziente dato il suo codice fiscale
     *
     * @param codiceFiscale
     * @return paziente
     * @throws SQLException
     */
    public Paziente getPazienteByCodiceFiscale(String codiceFiscale) throws SQLException {

        connetti();

        Paziente paziente;
        paziente = new Paziente();
        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM pazienti Where CODICE_FISCALE = '" + codiceFiscale + "'")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setE_mail(rs.getString("e_mail"));
                    paziente.setNome(rs.getString("nome"));
                    paziente.setCognome(rs.getString("cognome"));
                    paziente.setCodice_fiscale(rs.getString("codice_fiscale"));
                    paziente.setData_di_nascita(rs.getString("data_di_nascita"));
                    paziente.setLuogo_di_nascita(rs.getString("luogo_di_nascita"));
                    paziente.setSesso(rs.getString("sesso"));
                    paziente.setProvincia(rs.getString("provincia"));

                }
            }
            shutdown();
        }

        return paziente;
    }

    /**
     * Funzione per modificare password, email o medico di base
     *
     * @param email
     * @param medicoId
     * @param userId
     * @param password
     * @throws SQLException
     */
    public void modifyPaziente(String email, int medicoId, int userId, String password) throws SQLException {

        try {

            connetti();

            // create our java preparedstatement using a sql update query
            // set the preparedstatement parameters
            PreparedStatement ps = null;

            if (medicoId != -1) {
                ps = conn.prepareStatement("UPDATE PAZIENTI SET MEDICO_DI_BASE = ? , E_MAIL = ?, PASSWORD = ? WHERE ID_PAZIENTE = ?");
                ps.setInt(1, medicoId);
                ps.setString(2, email);
                ps.setString(3, password);
                ps.setInt(4, userId);
            } else {
                ps = conn.prepareStatement("UPDATE PAZIENTI SET E_MAIL = ?, PASSWORD = ? WHERE ID_PAZIENTE = ?");
                ps.setString(1, email);
                ps.setString(2, password);
                ps.setInt(3, userId);

            }

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            // log the exception
            throw se;
        }

        shutdown();

    }

    /**
     * Funzione per aggiornare l'immagine di profilo dell'utente
     *
     * @param user
     * @param nome
     * @throws SQLException
     */
    public void UpdateImg(Paziente user, String nome) throws SQLException {

        try {

            connetti();

            // create our java preparedstatement using a sql update query
            // set the preparedstatement parameters
            PreparedStatement ps = null;

            ps = conn.prepareStatement("UPDATE PAZIENTI SET AVATAR_PATH = ? WHERE ID_PAZIENTE = ?");
            ps.setString(1, nome);
            ps.setInt(2, user.getId());

            // call executeUpdate to execute our sql update statement
            ps.executeUpdate();
            ps.close();
        } catch (SQLException se) {
            // log the exception
            throw se;
        }

        shutdown();

    }

    /**
     * Funzione di controllo 
     * @param nome
     * @param cognome
     * @param codice_fiscale
     * @return
     * @throws SQLException
     */
    public Paziente ValidatePaziente(String nome, String cognome, String codice_fiscale) throws SQLException {

        connetti();

        Paziente paziente = null;
        paziente = new Paziente();

        try (PreparedStatement stm = conn.prepareStatement("Select * From pazienti Where nome = '" + nome + "' and cognome = '" + cognome + "' and codice_fiscale = '" + codice_fiscale + "'")) {
            try (ResultSet rs = stm.executeQuery()) {
                while (rs.next()) {
                    paziente.setId_paziente(rs.getInt("id_paziente"));
                    paziente.setMedico_di_base(rs.getInt("Medico_di_base"));
                    paziente.setE_mail(rs.getString("e_mail"));
                }

                shutdown(); // é assosulatamente vitale che lui sia dopo qualsiasi operazione sul ResultSet

            }

        }
        return paziente;
    }
}
