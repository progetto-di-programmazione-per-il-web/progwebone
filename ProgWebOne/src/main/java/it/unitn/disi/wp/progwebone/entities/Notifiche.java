package it.unitn.disi.wp.progwebone.entities;

/**
 * Rappresenta le notifiche di sistema
 *
 * @author ProgWebOne
 *
 */
public class Notifiche {

    private int id_paziente;
    private int numero_visite;
    private int numero_esami;
    private int numero_ricette;

    /**
     * Dati:
     *
     * @param id_paziente rappresenta id del paziente
     * @param numero_visite rappresenta numero di notifiche di tipo visita
     * @param numero_esami rappresenta numero di notifiche di tipo esame
     * @param numero_ricette rappresenta numero di notifiche di tipo ricetta
     *
     */
    /**
     * Metodo getter
     *
     * @return id_paziente
     */
    public int getId_paziente() {
        return id_paziente;
    }

    /**
     * Metodo setter
     *
     * @param id_paziente
     */
    public void setId_paziente(int id_paziente) {
        this.id_paziente = id_paziente;
    }

    /**
     * Metodo getter
     *
     * @return numero_visite
     */
    public int getNumero_visite() {
        return numero_visite;
    }

    /**
     * Metodo setter
     *
     * @param numero_visite
     */
    public void setNumero_visite(int numero_visite) {
        this.numero_visite = numero_visite;
    }

    /**
     * Metodo getter
     *
     * @return numero_esami
     */
    public int getNumero_esami() {
        return numero_esami;
    }

    /**
     * Metodo setter
     *
     * @param numero_esami
     */
    public void setNumero_esami(int numero_esami) {
        this.numero_esami = numero_esami;
    }

    /**
     * Metodo getter
     *
     * @return numero_ricette
     */
    public int getNumero_ricette() {
        return numero_ricette;
    }

    /**
     *
     * @param numero_ricette
     */
    public void setNumero_ricette(int numero_ricette) {
        this.numero_ricette = numero_ricette;
    }

    /**
     *
     *
     *
     * @return la notifica in formato string
     */

    @Override
    public String toString() {
        return "Notifiche{" + "id_paziente=" + id_paziente + ", numero_visite=" + numero_visite + ", numero_esami=" + numero_esami + ", numero_ricette=" + numero_ricette + '}';
    }

}
