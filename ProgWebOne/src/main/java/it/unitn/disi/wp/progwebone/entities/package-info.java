/**
 * Provvede le entità del progetto
 * <p>
 * Contiene 8 entità:
 * <p>
 * {@link it.unitn.disi.wp.progwebone.entities.Esame},
 * {@link it.unitn.disi.wp.progwebone.entities.EsamePossibile},
 * {@link it.unitn.disi.wp.progwebone.entities.Medico},
 * {@link it.unitn.disi.wp.progwebone.entities.Notifiche},
 * {@link it.unitn.disi.wp.progwebone.entities.Paziente},
 * {@link it.unitn.disi.wp.progwebone.entities.Ricetta},
 * {@link it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale},
 * {@link it.unitn.disi.wp.progwebone.entities.Visita}
 *
 * <p>
 * Le varie classi contengono metodi getter and setter per inizializzare e
 * prelevare dati dagli oggetti
 *
 * @author ProgWebOne
 * @since 1.0
 * @see it.unitn.disi.wp.progwebone.entities
 */
package it.unitn.disi.wp.progwebone.entities;
