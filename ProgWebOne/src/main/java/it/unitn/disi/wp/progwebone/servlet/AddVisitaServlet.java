/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.EsamePossibile;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsamiPossibili;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import it.unitn.disi.wp.progwebone.utilityFunction.sendEmail;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *  Servlet che gestisce l'aggiunta di visite, l'invio di mail e update di notifiche di tipo visita.
 * 
 * @author ProgWebOne
 */
public class AddVisitaServlet extends HttpServlet {

    
    
       
    /**
     * 
     * Gestisce l'aggiunta di visite, l'invio di mail e update di notifiche di tipo visita.
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
    */
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        Medico user = (Medico) request.getSession().getAttribute("user");
        int id_paziente = Integer.valueOf(request.getParameter("id_paziente"));
        String visita = request.getParameter("Visita");
        int id_esame = Integer.parseInt(request.getParameter("Esame_prescritto"));
        String Esame_Prescritto = "Nessuno";
        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilEsamiPossibili dbEsamiPossibili = new DBUtilEsamiPossibili();
        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();

        List<EsamePossibile> esami_possibili;
        esami_possibili = new ArrayList<>();
        Paziente paziente = new Paziente();

        try {
            esami_possibili = dbEsamiPossibili.getEsamiPossibili();
            paziente = dbPaziente.getPazienteById(id_paziente);
        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (id_esame != -1) {
            for (EsamePossibile esaTmp : esami_possibili) {
                if (id_esame == esaTmp.getId_esame()) {
                    Esame_Prescritto = esaTmp.getEsame();
                }
            }
        }

        if (paziente.getMedicoDiBase() == user.getId_medico()) {
            try {
                dbVisite.InserisciVisita(id_paziente, user.getId_medico(), visita, Esame_Prescritto);
                dbNotifiche.updateNotifiche(id_paziente, 1, 0, 0);
                sendEmail.send(paziente.getEmail(), "Ti è stata eseguita una nuova visita");

                if (id_esame != -1) {
                    dbEsami.InserisciEsame(id_paziente, user.getId_medico(), Esame_Prescritto);
                    dbNotifiche.updateNotifiche(id_paziente, 0, 1, 0);
                    sendEmail.send(paziente.getEmail(), "Ti è stato prescritto un nuovo esame");
                }
            } catch (SQLException ex) {
                Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "DatiPaziente?id=" + id_paziente));
        } else {
            response.sendError(500);
            return;
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
