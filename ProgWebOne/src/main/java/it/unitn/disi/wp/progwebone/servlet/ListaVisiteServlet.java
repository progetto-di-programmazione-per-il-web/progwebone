package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Notifiche;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilNotifiche;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede la liste delle visite.
 *
 * @author ProgWebOne
 */
public class ListaVisiteServlet extends HttpServlet {
/**
 * Richiede la liste delle visite e la inserisce nella request.
 * @param request
 * @param response
 * @throws ServletException 
 */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaVisite(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            listaVisite(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    private void listaVisite(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<Visita> visite = new ArrayList<>();

        DBUtilNotifiche dbNotifiche = new DBUtilNotifiche();
        DBUtilVisite dbVisite = new DBUtilVisite();

        Paziente user = (Paziente) request.getSession().getAttribute("user");
        Notifiche notifica = null;

        try {
            visite = dbVisite.getVisite(user.getId());
            notifica = dbNotifiche.removeNotifica(user.getId(), 0);
        } catch (SQLException ex) {
            Logger.getLogger(ListaVisiteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //aggiungo i pazienti alla request
        request.setAttribute("LISTA_VISITE", visite);
        request.getSession().setAttribute("notifica", notifica);

        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("listaVisite.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
