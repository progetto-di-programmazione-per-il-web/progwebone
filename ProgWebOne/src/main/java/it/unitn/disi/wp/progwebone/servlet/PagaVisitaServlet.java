package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet per pagare una determinata visita
 *
 * @author ProgWebOne
 */
public class PagaVisitaServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            pagaVisita(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            pagaVisita(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    /**
     * Paga una determinata visita
     *
     * @param request
     * @param response
     * @throws Exception
     */
    private void pagaVisita(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Paziente user = (Paziente) request.getSession().getAttribute("user");
        Integer id_visita = null;

        try {
            id_visita = Integer.valueOf(request.getParameter("id"));
        } catch (RuntimeException ex) {
            //TODO: Handle the exception
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        DBUtilVisite dbVisite = new DBUtilVisite();
        Visita visita = null;
        visita = new Visita();

        try {
            visita = dbVisite.getVisitaById(id_visita);

        } catch (SQLException ex) {
            Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (visita.getId_paziente() == user.getId() && visita.getImporto_pagato() == 0) {
            try {
                dbVisite.pagaVisita(id_visita);

            } catch (SQLException ex) {
                Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "ListaVisite"));
        } else {
            response.sendError(500);
            return;
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
