
/**
 * Provvede un filter per controllare se in sessione è definito un user
 * <p>
 * Contiene 1 classe: 
 * <p>
 * {@link it.unitn.disi.wp.progwebone.filters.ValidationFilter}

 * 
 * @author ProgWebOne
 * @since 1.0
 * @see it.unitn.disi.wp.progwebone.filters
 */
package it.unitn.disi.wp.progwebone.filters;