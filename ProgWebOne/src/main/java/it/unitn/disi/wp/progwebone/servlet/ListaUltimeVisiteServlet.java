package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.EsamePossibile;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsamiPossibili;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che gestisce la landing del medico, richiede le ultime visite, esami prescritti e la lista dei pazienti.
 *
 * @author ProgWebOne
 */
public class ListaUltimeVisiteServlet extends HttpServlet {

    /**
     * Richiede le ultime visite, gli esami prescritti, la lista dei pazienti e le inserisce nella
     * request.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    private void listaUltimeVisite(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Medico user = (Medico) request.getSession(false).getAttribute("user");
        List<Visita> visite = new ArrayList<>();
        List<Ricetta> ricette = new ArrayList<>();
        List<EsamePossibile> esami_possibili = new ArrayList<>();
        List<Paziente> pazienti = new ArrayList<>();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();
        DBUtilEsamiPossibili dbEsamiPossibili = new DBUtilEsamiPossibili();
        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilRicette dbRicette = new DBUtilRicette();

        try {
            visite = dbVisite.getVisiteByMedico(user.getId_medico());
            ricette = dbRicette.getRicetteByMedico(user.getId_medico());
            esami_possibili = dbEsamiPossibili.getEsamiPossibili();
            pazienti = dbPaziente.getPazienti(user.getId_medico());
        } catch (SQLException ex) {
            Logger.getLogger(ListaVisiteServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        //aggiungo le visite alla request
        request.setAttribute("LISTA_VISITE", visite);
        request.setAttribute("LISTA_RICETTE", ricette);
        request.setAttribute("ESAMI_POSSIBILI", esami_possibili);
        request.setAttribute("LISTA_PAZIENTI", pazienti);

        //mando alla jsp (view) 
        RequestDispatcher dispatcher = request.getRequestDispatcher("landingMedico.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        listaUltimeVisite(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        listaUltimeVisite(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
