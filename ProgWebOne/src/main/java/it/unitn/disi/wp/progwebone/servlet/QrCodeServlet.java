package it.unitn.disi.wp.progwebone.servlet;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;
import be.quodlibet.boxable.utils.PDStreamUtils;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilMedico;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilRicette;
import java.sql.SQLException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 * Servlet per creare un pdf contenente un QR e i dati di un farmaco prescritto
 * per uno specifico paziente
 *
 * @author ProgWebOne
 */
public class QrCodeServlet extends HttpServlet {

    /**
     * Creare un pdf contenente un QR e i dati di un farmaco prescritto per uno
     * specifico paziente
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pdfFolder = "pdfFolder";
        pdfFolder = getServletContext().getRealPath(pdfFolder);

        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        try {

            DBUtilRicette dbRicette = new DBUtilRicette();
            DBUtilMedico dbMedico = new DBUtilMedico();
            Ricetta ricetta = new Ricetta();
            Medico medico = new Medico();

            try {
                ricetta = dbRicette.getRicetta(user.getId(), Integer.valueOf(request.getParameter("id")));
                medico = dbMedico.getMedicoByIdMedico(user.getMedicoDiBase());
            } catch (SQLException ex) {
                Logger.getLogger(QrCodeServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            String TestoQr = "Ricetta per: " + user.getCognome() + " " + user.getNome() + "\nCodice Fiscale: " + user.getCodiceFiscale() + "\nPrescritta da: " + medico.getCognome() + " " + medico.getNome() + "\nId Medico: " + medico.getId_medico()
                    + "\nRicetta: " + ricetta.getFarmaco() + "\nId ricetta: " + ricetta.getId_ricetta() + "\nData di prescrizione: " + String.valueOf(ricetta.getTimestamp()).substring(0, 19);

            ByteArrayOutputStream out = QRCode.from(TestoQr).to(ImageType.JPG).stream();
            String path = pdfFolder + "QR" + user.getCognome() + ".jpg";

            FileOutputStream fout = new FileOutputStream(new File(path));
            fout.write(out.toByteArray());
            fout.flush();
            fout.close();

            PDDocument doc = new PDDocument();
            PDPage page = new PDPage();
            doc.addPage(page);

            //Creating PDImageXObject object
            PDImageXObject pdImage = PDImageXObject.createFromFile(path, doc);

            //creating the PDPageContentStream object
            PDPageContentStream contents = new PDPageContentStream(doc, page);

            PDStreamUtils.write(
                    contents,
                    "Prescrizone Farmaco per " + user.getNome() + " " + user.getCognome(),
                    PDType1Font.HELVETICA_BOLD,
                    14,
                    80,
                    625,
                    Color.BLACK);

            PDStreamUtils.write(
                    contents,
                    "(Cod Fiscale: " + user.getCodiceFiscale() + ")",
                    PDType1Font.HELVETICA_BOLD,
                    14,
                    80,
                    600,
                    Color.BLACK);

            PDStreamUtils.write(
                    contents,
                    "Farmaco: " + ricetta.getFarmaco() + "   " + "Id farmaco:" + ricetta.getId_ricetta(),
                    PDType1Font.HELVETICA_BOLD,
                    12,
                    80,
                    575,
                    Color.BLACK);

            PDStreamUtils.write(
                    contents,
                    "Dottor: " + medico.getCognome() + " " + medico.getNome(),
                    PDType1Font.HELVETICA_BOLD_OBLIQUE,
                    12,
                    400,
                    450,
                    Color.BLACK);

            //Drawing the image in the PDF document
            contents.drawImage(pdImage, 425, 650);

            //Closing the PDPageContentStream object
            contents.close();

            doc.save(new File(pdfFolder, "userQR-" + user.getNome() + "-" + Calendar.getInstance().getTimeInMillis() + ".pdf"));

            response.setContentType("application/pdf");
            response.setHeader("Content-disposition", "attachment; filename=RicettaQr.pdf");
            doc.save(response.getOutputStream());

            //Closing the document
            doc.close();

        } catch (FileNotFoundException e) {
            Logger.getLogger(QrCodeServlet.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(QrCodeServlet.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
