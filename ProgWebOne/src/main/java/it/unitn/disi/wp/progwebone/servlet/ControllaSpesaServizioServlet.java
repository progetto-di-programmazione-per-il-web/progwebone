package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilMedico;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *  Servlet che gestisce la spesa per il servizio provinciale  
 * @author ProgWebOne
 */
public class ControllaSpesaServizioServlet extends HttpServlet {

    /**
     * Inserisce nella request tutti i ticket pagati in provincia
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    protected void getServiziErogati(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();
        DBUtilMedico dbMedico = new DBUtilMedico();

        List<Esame> listEsami = new ArrayList();
        List<Visita> listVisite = new ArrayList();
        List<Medico> listMedico;
        ServizioSanitarioProvinciale user = (ServizioSanitarioProvinciale) request.getSession().getAttribute("user");
        int sommaVisite = 0;
        int sommaEsami = 0;
        int totale = 0;

        try {
            listMedico = dbMedico.getMedicoByProvincia(user.getProvincia());

            for (Medico medTmp : listMedico) {

                List<Visita> listVisiteTmp = null;
                List<Esame> listEsamiTmp = null;

                listVisiteTmp = dbVisite.getVisiteByMedico(medTmp.getId_medico());
                listEsamiTmp = dbEsami.getEsamiByMedico(medTmp.getId_medico());

                for (Visita visitaTmp : listVisiteTmp) {

                    if (visitaTmp.getImporto_pagato() > 0) {
                        sommaVisite += visitaTmp.getImporto_pagato();
                        listVisite.add(visitaTmp);

                    }

                }

                for (Esame esameTmp : listEsamiTmp) {

                    if (esameTmp.getImporto_pagato() > 0) {
                        sommaEsami += esameTmp.getImporto_pagato();
                        listEsami.add(esameTmp);

                    }

                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(ControllaSpesaServizioServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        totale = sommaEsami + sommaVisite;

        request.setAttribute("Lista_visite", listVisite);
        request.setAttribute("Lista_esami", listEsami);
        request.setAttribute("totale", totale);

        RequestDispatcher dispatcher = request.getRequestDispatcher("finanzeServizio.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServiziErogati(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getServiziErogati(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
