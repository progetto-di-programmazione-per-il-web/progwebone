package it.unitn.disi.wp.progwebone.utilityFunction;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Classe per mandare in automatico e-mail al paziente al quale è stato prescritto qualcosa
 *
 * @author ProgWebOne
 *
 */
public class sendEmail {

    /**
     * Data in input email e testo, la funzione manda in automatico al destinario con determinato testo 
     * @param email
     * @param testo
     */
    public static void send(String email, String testo) {
        final String host = "smtp.gmail.com";
        final String port = "465";
        final String username = "notifiche.servizio.noanswer@gmail.com";
        final String password = "rsifplrtbrgjhkmz";
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.socketFactory.port", port);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.debug", "true");

        Session session = Session.getInstance(props, new Authenticator() {

            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message msg = new MimeMessage(session);
        try {
            msg.setFrom(new InternetAddress(username));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            msg.setSubject("Nuova Prescrizione");
            msg.setText(testo + "\n\n\n\n"
                    + "Questo messaggio e' stato generato automaticamente, La preghiamo pertanto di non rispondere a questa e-mail poiche' l'indirizzo non e' controllato.\n"
                    + "Per qualsiasi chiarimento puo' contattare il proprio medico di base dal lunedi' al venerdi' dalle 8.00 alle 20.00, il sabato dalle 9.00 alle 14.00 telefonando al numero 199-555123442");

            msg.setSentDate(new Date());
            Transport.send(msg);
        } catch (MessagingException me) {
//TODO: log the exception
            me.printStackTrace(System.err);
        }
    }

}
