/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progwebone.utilityFunction;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *Classe per criptare in SHA256
 * 
 * @author ProgWebOne
 */
public class Sha256 {

    
/**
 * Restituisce una stringa criptata dopo averne ricevuta una in input
 * @param pass
 * @return stringa criptata
 */    
public String encrypter(String pass){
    
    String sha256hex;
   
    sha256hex =  DigestUtils.sha256Hex(pass);
    
    return sha256hex;
}
    

}
