package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.Visita;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilVisite;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che richiede la lista dei ticket pagati da un paziente
 *
 * @author ProgWebOne
 */
public class ListaTicketPagatiServlet extends HttpServlet {

    /**
     * Richiede la lista dei ticket pagati da un paziente e la inserisce nella request
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    protected void ListaTicket(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        List<Visita> listaVisiteTmp = new ArrayList<>();
        List<Esame> listeEsameTmp = new ArrayList<>();

        Paziente user = (Paziente) request.getSession().getAttribute("user");

        DBUtilVisite dbVisite = new DBUtilVisite();
        DBUtilEsami dbEsami = new DBUtilEsami();

        List<Visita> listaVisite = new ArrayList<>();
        List<Esame> listeEsame = new ArrayList<>();

        try {

            listaVisite = dbVisite.getVisite(user.getId());
            listeEsame = dbEsami.getEsami(user.getId());

            for (Esame tmp : listeEsame) {

                if (tmp.getImporto_pagato() > 0) {

                    listeEsameTmp.add(tmp);

                }

            }
            for (Visita tmp : listaVisite) {

                if (tmp.getImporto_pagato() > 0) {

                    listaVisiteTmp.add(tmp);

                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(ListaTicketPagatiServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("LISTA_VISITE", listaVisiteTmp);
        request.setAttribute("LISTA_ESAMI", listeEsameTmp);

        RequestDispatcher dispatcher = request.getRequestDispatcher("ticketPagati.jsp");
        dispatcher.forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ListaTicket(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ListaTicket(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
