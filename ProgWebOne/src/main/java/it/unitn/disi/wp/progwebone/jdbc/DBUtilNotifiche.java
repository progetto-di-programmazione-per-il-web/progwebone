package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Notifiche;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Notifiche}
 *
 * @author ProgWebOne
 */
public class DBUtilNotifiche extends DBUtil {

    /**
     * Funzione per recuperare tutte le notifiche di un determinato paziente
     * @param id
     * @return
     * @throws SQLException
     */
    public Notifiche getNotifiche(int id) throws SQLException {
        connetti();

        Notifiche notifica = new Notifiche();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM notifiche where id_paziente =" + id)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {

                    notifica.setId_paziente(rs.getInt("id_paziente"));
                    notifica.setNumero_esami(rs.getInt("numero_esami"));
                    notifica.setNumero_ricette(rs.getInt("numero_ricette"));
                    notifica.setNumero_visite(rs.getInt("numero_visite"));

                }
                shutdown();
            }
        }

        return notifica;
    }

    /** Funzione per inserire una nuova notifica utilizzando id del paziente e specificando quando notifiche assegnargli
     * @param id_paziente
     * @param visita
     * @param esame
     * @param ricetta
     * @return
     * @throws SQLException
     */
    public Notifiche updateNotifiche(int id_paziente, int visita, int esame, int ricetta) throws SQLException {

        Notifiche notifica;
        notifica = getNotifiche(id_paziente);

        connetti();

        PreparedStatement ps;

        ps = conn.prepareStatement("UPDATE notifiche SET numero_visite = ? ,  numero_esami = ? , numero_ricette = ? WHERE ID_PAZIENTE = ?");

        visita = visita + notifica.getNumero_visite();
        esame = esame + notifica.getNumero_esami();
        ricetta = ricetta + notifica.getNumero_ricette();

        ps.setInt(1, visita);
        ps.setInt(2, esame);
        ps.setInt(3, ricetta);
        ps.setInt(4, id_paziente);

        // call executeUpdate to execute our sql update statement
        ps.executeUpdate();
        ps.close();

        notifica.setNumero_esami(esame);
        notifica.setNumero_ricette(ricetta);
        notifica.setNumero_visite(visita);

        shutdown();

        return notifica;
    }

    /** Funzione che rimuove le notifiche dopo averle visualizzate in base all'id del paziente e la tipologia di notifica
     *
     * @param id_paziente
     * @param tipo
     * @return
     * @throws SQLException
     */
    public Notifiche removeNotifica(int id_paziente, int tipo) throws SQLException {

        Notifiche notifica;
        notifica = getNotifiche(id_paziente);

        connetti();

        PreparedStatement ps = null;

        if (tipo == 0) {
            ps = conn.prepareStatement("UPDATE notifiche SET numero_visite = ? WHERE ID_PAZIENTE = ?");

            notifica.setNumero_visite(0);
        }
        if (tipo == 1) {
            ps = conn.prepareStatement("UPDATE notifiche SET numero_esami =  ? WHERE ID_PAZIENTE = ?");
            notifica.setNumero_esami(0);

        }
        if (tipo == 2) {
            ps = conn.prepareStatement("UPDATE notifiche SET numero_ricette =  ?  WHERE ID_PAZIENTE = ?");

            notifica.setNumero_ricette(0);
        }

        ps.setInt(1, 0);

        ps.setInt(2, id_paziente);

        // call executeUpdate to execute our sql update statement
        ps.executeUpdate();
        ps.close();

        shutdown();

        return notifica;
    }
}
