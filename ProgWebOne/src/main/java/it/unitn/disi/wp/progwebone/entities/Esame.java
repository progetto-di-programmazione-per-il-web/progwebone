package it.unitn.disi.wp.progwebone.entities;

import java.sql.Timestamp;

/**
 * Rappresenta i dati di un esame
 *
 * @author ProgWebOne
 *
 */
public class Esame {

    private int id_esame;
    private int id_paziente;
    private int id_medico;
    private String esame;
    private String esito;
    private String pagato;
    private int importo_pagato;
    private int eseguito;
    private Timestamp timestamp;

    /**
     * Dati:
     * @param id_esame rappresenta id univoco dell'esame
     * @param id_paziente rappresenta id specifico del paziente
     * @param id_medico rappresenta id specifico del paziente
     * @param esame stringa che rappresenta l'esame da eseguire
     * @param esito stringa che rappresenta l'esito del esame (esempio: positivo o  negativo)
     * @param pagato 
     * @param importo_pagato  rappresenta l'importo pagato per l'esame
     * @param eseguito rappresenta se è stato eseguito o meno (1 o 0)
     * @param timestamp  rappresenta data in cui è stato prescritto 
     */
    
    
    /**
     * Metodo getter
     *
     * @return eseguito
     */
    public int getEseguito() {
        return eseguito;
    }

    /**
     * Metodo setter
     *
     * @param eseguito
     */
    public void setEseguito(int eseguito) {
        this.eseguito = eseguito;
    }

    /**
     * Metodo getter
     *
     * @return importo_pagato
     */
    public int getImporto_pagato() {
        return importo_pagato;
    }

    /**
     * Metodo setter
     *
     * @param importo_pagato
     */
    public void setImporto_pagato(int importo_pagato) {
        this.importo_pagato = importo_pagato;
    }

    /**
     * Metodo getter
     *
     * @return pagato
     */
    public String getPagato() {
        return pagato;
    }

    /**
     * Metodo setter
     *
     * @param pagato
     */
    public void setPagato(String pagato) {
        this.pagato = pagato;
    }

    /**
     * Metodo getter
     *
     * @return timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Metodo setter
     *
     * @param timestamp
     */
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Inizializzatore vuoto
     */
    public Esame() {

    }

    /**
     * Metodo getter
     *
     * @return id_esame
     */
    public int getId_esame() {
        return id_esame;
    }

    /**
     * Metodo setter
     *
     * @param id_esame
     */
    public void setId_esame(int id_esame) {
        this.id_esame = id_esame;
    }

    /**
     * Metodo getter
     *
     * @return id_paziente
     */
    public int getId_paziente() {
        return id_paziente;
    }

    /**
     * Metodo setter
     *
     * @param id_paziente
     */
    public void setId_paziente(int id_paziente) {
        this.id_paziente = id_paziente;
    }

    /**
     * Metodo getter
     *
     * @return id_medico
     */
    public int getId_medico() {
        return id_medico;
    }

    /**
     * Metodo setter
     *
     * @param id_medico
     */
    public void setId_medico(int id_medico) {
        this.id_medico = id_medico;
    }

    /**
     * Metodo getter
     *
     * @return
     */
    public String getEsame() {
        return esame;
    }

    /**
     * Metodo setter
     *
     * @param esame
     */
    public void setEsame(String esame) {
        this.esame = esame;
    }

    /**
     * Metodo getter
     *
     * @return
     */
    public String getEsito() {
        return esito;
    }

    /**
     * Metodo setter
     *
     * @param esito
     */
    public void setEsito(String esito) {
        this.esito = esito;
    }

}
