package it.unitn.disi.wp.progwebone.servlet;

import it.unitn.disi.wp.progwebone.entities.Esame;
import it.unitn.disi.wp.progwebone.entities.Paziente;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilEsami;
import it.unitn.disi.wp.progwebone.jdbc.DBUtilPaziente;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet che gestisce l'esecuzione di esami da parte del servizio sanitario
 * @author ProgWebOne
 */
public class EseguiEsameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            eseguiEsame(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        try {
            eseguiEsame(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }

    }
/**
 * Esegue l'esame specifico di un determinato paziente
 * 
 * @param request
 * @param response
 * @throws Exception 
 */
    private void eseguiEsame(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ServizioSanitarioProvinciale user = (ServizioSanitarioProvinciale) request.getSession().getAttribute("user");
        Integer id_esame = null;
        Integer id_paziente = null;
        String esito = request.getParameter("Esito");
        Paziente paziente = new Paziente();
        Esame esame = new Esame();

        try {
            id_esame = Integer.valueOf(request.getParameter("id_e"));
            id_paziente = Integer.valueOf(request.getParameter("id_p"));
        } catch (RuntimeException ex) {
            //TODO: Handle the exception
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        DBUtilEsami dbEsame = new DBUtilEsami();
        DBUtilPaziente dbPaziente = new DBUtilPaziente();

        try {
            paziente = dbPaziente.getPazienteById(id_paziente);
            esame = dbEsame.getEsameById(id_esame);
        } catch (SQLException ex) {
            Logger.getLogger(AddVisitaServlet.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (paziente.getId_servizio() == user.getId() && esame.getId_paziente() == paziente.getId() && esame.getEseguito() == 0) {
            try {
                dbEsame.eseguiEsame(id_esame, esito);

            } catch (SQLException ex) {
                Logger.getLogger(ModificaDatiServlet.class.getName()).log(Level.SEVERE, null, ex);
            }

            response.sendRedirect(response.encodeRedirectURL(contextPath + "DatiPaziente?id=" + id_paziente));
        }else{
            response.sendError(500);
            return;
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
