package it.unitn.disi.wp.progwebone.jdbc;

import it.unitn.disi.wp.progwebone.entities.Medico;
import it.unitn.disi.wp.progwebone.entities.Ricetta;
import it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe contenente le query per ottenere entità di tipo
 * {@link it.unitn.disi.wp.progwebone.entities.Ricetta}
 *
 * @author ProgWebOne
 */
public class DBUtilRicette extends DBUtilMedico {

    /**
     * Ritorna lista di ricette appartenente ad un determinato paziente
     *
     * @param id_paziente
     * @return
     * @throws SQLException
     */
    public List<Ricetta> getRicette(int id_paziente) throws SQLException {

        connetti();

        List<Ricetta> ricette;
        ricette = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM ricette where id_paziente = " + id_paziente)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Ricetta ricettaTmp = new Ricetta();

                    ricettaTmp.setId_ricetta(rs.getInt("id_ricetta"));
                    ricettaTmp.setId_medico(rs.getInt("id_medico"));
                    ricettaTmp.setId_paziente(rs.getInt("id_paziente"));
                    ricettaTmp.setFarmaco(rs.getString("farmaco"));
                    ricettaTmp.setTimestamp(rs.getTimestamp("data_ora"));

                    ricette.add(ricettaTmp);
                }

                shutdown();
            }
        }

        return ricette;

    }

    /**
     * Ritorna lista di ricette prescritte da un determinato medico
     *
     * @param id_medico
     * @return
     * @throws SQLException
     */
    public List<Ricetta> getRicetteByMedico(int id_medico) throws SQLException {

        connetti();

        List<Ricetta> ricette;
        ricette = new ArrayList<>();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM ricette where id_medico = " + id_medico + "ORDER BY data_ora DESC")) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {
                    Ricetta ricettaTmp = new Ricetta();

                    ricettaTmp.setId_ricetta(rs.getInt("id_ricetta"));
                    ricettaTmp.setId_medico(rs.getInt("id_medico"));
                    ricettaTmp.setId_paziente(rs.getInt("id_paziente"));
                    ricettaTmp.setFarmaco(rs.getString("farmaco"));
                    ricettaTmp.setTimestamp(rs.getTimestamp("data_ora"));

                    ricette.add(ricettaTmp);
                }

                shutdown();
            }
        }

        return ricette;

    }

    /**
     * Funzione per inserire un farmaco nel database specifando il paziente, il medico e il farmaco.
     *
     * @param ID_PAZIENTE
     * @param ID_MEDICO
     * @param FARMACO
     * @throws SQLException
     */
    public void InserisciFarmaco(int ID_PAZIENTE, int ID_MEDICO, String FARMACO) throws SQLException {

        connetti();

        String sql = "insert into ricette ( ID_PAZIENTE, ID_MEDICO, FARMACO, DATA_ORA) VALUES (?,?,?,?)";
        // create the sql insert preparedstatement
        PreparedStatement preparedStmt = conn.prepareStatement(sql);
        preparedStmt.setInt(1, ID_PAZIENTE);
        preparedStmt.setInt(2, ID_MEDICO);
        preparedStmt.setString(3, FARMACO);
        preparedStmt.setTimestamp(4, Timestamp.from(Instant.now()));

        preparedStmt.executeUpdate();

        shutdown();
    }

    /** Ritorna una ricetta tramite id e con controllo id del paziente
     *
     * @param idPaziente
     * @param ricettaId
     * @return ricetta
     * @throws SQLException
     */
    public Ricetta getRicetta(int idPaziente, int ricettaId) throws SQLException {

        connetti();

        Ricetta ricetta = new Ricetta();

        try (PreparedStatement stm = conn.prepareStatement("SELECT * FROM ricette where id_paziente = " + idPaziente + " AND id_ricetta = " + ricettaId)) {
            try (ResultSet rs = stm.executeQuery()) {

                while (rs.next()) {

                    ricetta.setId_ricetta(rs.getInt("id_ricetta"));
                    ricetta.setId_medico(rs.getInt("id_medico"));
                    ricetta.setId_paziente(rs.getInt("id_paziente"));
                    ricetta.setFarmaco(rs.getString("farmaco"));
                    ricetta.setTimestamp(rs.getTimestamp("data_ora"));

                }

                shutdown();
            }
        }

        return ricetta;

    }

    /**
     * Ritorna una lista di ricette prescritte in una determinata provincia in un determinato giorno
     * @param data
     * @param user
     * @return lista di ricette
     * @throws SQLException
     */
    public List<Ricetta> getRicetteByDateAndProvincia(String data, ServizioSanitarioProvinciale user) throws SQLException {

        connetti();

        List<Ricetta> ricette;
        ricette = new ArrayList<>();

        List<Medico> listaMedici;
        listaMedici = new ArrayList<>();

        listaMedici = getMedicoByProvincia(user.getProvincia());

        for (Medico medTmp : listaMedici) {

            List<Ricetta> listaRicetteTmp = null;

            listaRicetteTmp = getRicetteByMedico(medTmp.getId_medico());

            for (Ricetta ricettaTmp : listaRicetteTmp) {

                if (String.valueOf(ricettaTmp.getTimestamp()).substring(0, 10).equals(data)) {
                    ricette.add(ricettaTmp);
                }
            }

        }

        shutdown();

        return ricette;
    }
    
}
