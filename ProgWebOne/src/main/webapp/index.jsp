<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Titolo -->
        <title>Servizio Sanitario Provinciale</title>

        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
        <script type="text/javascript" src="/8F424C45-4022-AA47-BD33-99EE8AA8C5CC/main.js" charset="UTF-8"></script><script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
        <script  src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" charset="utf-8"></script>
    </head>


    <%
        Cookie[] cookies = request.getCookies();
        String userName = "", password = "", rememberVal = "", tipo = "a";
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("cookuser")) {
                    userName = cookie.getValue();
                }
                if (cookie.getName().equals("cookpass")) {
                    password = cookie.getValue();
                }
                if (cookie.getName().equals("cookrem")) {
                    rememberVal = cookie.getValue();
                }
                if (cookie.getName().equals("cookTipo")) {
                    tipo = cookie.getValue();
                }
            }
        }

        Integer errore = (Integer) request.getSession().getAttribute("erroreLogin");
        request.getSession().removeAttribute("erroreLogin");
    %>


    <!-- -------------------------------------------- body -------------------------------------------------- -->
    <body>
        <!-- ---------------------------------------- Navbar ------------------------------------------------ -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <span class="navbar-brand">Servizio Sanitario</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <!-- Bottone login -->
                <% if(userName == ""){ %>
                    <button class="btn btn-outline-info my-2 my-sm-0" type="submit" data-toggle="modal" data-target="#modalLoginForm">Login</button>
                <% }else{ %>
                    <form action="LoginServlet" method="POST">
                        <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Login</button>
                    </form>
                <% } %>
            </div>
        </nav>
        <!-- ------------------------------------------ /.navbar -------------------------------------------- -->
        <!-- ----------------------------------------- Carousel --------------------------------------------- -->
        <div class="bd-example">
            <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active" style="background-image: url(img/Flag_of_South_Tyrol.svg);">                    
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Servizio Sanitario di Bolzano</h5>                       
                        </div>
                    </div>
                    <div class="carousel-item" style="background-image: url(img/Flag_of_Trento_Province.svg);">                      
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Servizio Sanitario di Trento</h5>                         
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <!-- ---------------------------------------- /.carousel ---------------------------------------------- -->


        <!-- ---------------------------------------- contenitore --------------------------------------------- -->
        <div class="contenitore">
            <hr class="featurette-divider">
            <!--blockquote-->
            <div id="blockquote">
                <blockquote class="blockquote text-center">
                    <p class="mb-0" id="articolo_1_paragrafo_1">
                        "La tutela della salute come diritto fondamentale dell´individuo ed interesse della collettività è garantita, nel rispetto della dignità e della libertà 
                        della persona, dal Servizio sanitario nazionale, quale complesso delle funzioni e delle attività assistenziali dei Servizi sanitari regionali".
                    </p>
                    <footer class="blockquote-footer">art. 1 decreto legislativo n. 229 del 19 giugno 1999</footer>
                </blockquote>
            </div>
            <hr class="featurette-divider">
            <!-- /#blockquote -->
            <!--presentazione-->
            <div class="presentazione">
                <!--chi siamo-->
                <div id="chi_siamo">
                    <h1>Chi siamo</h1>
                    <p>
                        ProgWebOne è un servizio di gestione dei due sistemi sanitari provinciali del Trentino Alto-Adige: Trento e Bolzano. Il nostro obiettivo è
                        quello di rendere più semplice l'approccio alla sanità regionale e di rendere i servizi sanitari il più "completi" ed accessibili possibile ed a misura
                        del paziente riducendo le lunghe attese grazie alla loro informatizzazione.
                    </p>
                </div>

                <!--servizi-->
                <div id="servizi">
                    <h1>I nostri servizi</h1>
                    <p>
                        Un utente registrato, dopo aver fatto il login, può consultare nella sua area personale la lista dei propri esami o visite mediche già fatte o prenotate. 
                        Può vedere anche una lista con le ricette e le prescrizioni ricevute. Inoltre se ne avesse la necessità il cittadino puà cambiare medico di base in base alla
                        sua provincia di residenza. Il paziente riceverà un'e-mail ogni volta che gli viene prescritta una ricetta dal medico di base. Egli può anche stampare una 
                        ricetta farmaceutica e visualizzare tutti i ticket da lui pagati.
                    </p>
                    <p>
                        Un medico di base, in seguito alla sua registrazione al sistema, può visualizzare una lista di tutti i suoi pazienti e più nello specifico può vedere la
                        scheda paziente (esami e visite mediche fatte) e i dati personali. Il medico inoltre può prescrivere un esame medico, dei farmaci ed erogare una visita medica.
                    </p>
                    <p>
                        Il Servizio Sanitario Provinciale può erogare l'esame prescitto con un ticket di 11 euro.
                    </p>
                </div>
            </div>
            <!-- /#presentazione -->
            <!--colonne medici-->
            <hr class="featurette-divider">
            <div class="colonne_medici">
                <h1>I medici</h1>
                <div class="row" id="riga_med_1">
                    <div class="col-lg-4" id="colonna_medico">
                        <img src="./img/medici/medico_1.jpg" class="bd-placeholder-img rounded-circle" width="150" height="150" id="img_medico_1" alt="medico_1">
                        <address>
                            <h2>Pasquale Lucchesi</h2>
                            Via Druso, 40<br>
                            Bolzano, BZ 39100<br>
                            Phone: 123 456 7890<br>
                            <a href="mailto:#">pasquale.lucchesi@mail.com</a>
                        </address>
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4" id="colonna_medico">
                        <img src="./img/medici/medico_2.jpg" class="bd-placeholder-img rounded-circle" width="150" height="150" id="img_medico_2" alt="medico_2">
                        <address>
                            <h2>Milena Russo</h2>
                            Via Roma, 108<br>
                            Merano, BZ 39012<br>
                            Phone: 123 456 7890<br>
                            <a href="mailto:#">milena.russo@mail.com</a>
                        </address>                     
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4" id="colonna_medico">
                        <img src="./img/medici/medico_3.jpg" class="bd-placeholder-img rounded-circle" width="150" height="150" id="img_medico_3" alt="medico_3">
                        <address>
                            <h2>Gilberto Longo</h2>
                            Via del Brennero, 20<br>
                            Trento, TN 38122<br>
                            Phone: 123 456 7890<br>
                            <a href="mailto:#">gilberto.longo@mail.com</a>
                        </address>
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
                <div class="row" id="riga_med_2">
                    <div class="col-lg-6" id="colonna_medico">
                        <img src="./img/medici/medico_4.png" class="bd-placeholder-img rounded-circle" width="150" height="150" id="img_medico_4" alt="medico_4">
                        <address>
                            <h2>Eduardo Zetticci</h2>
                            Corso Antonio Rosmini, 14<br>
                            Rovereto, TN 38068<br>
                            Phone: 123 456 7890<br>
                            <a href="mailto:#">eduardo.zetticci@mail.com</a>
                        </address>
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6" id="colonna_medico">
                        <img src="./img/medici/medico_5.jpg" class="bd-placeholder-img rounded-circle" width="150" height="150" id="img_medico_5" alt="medico_5">
                        <address>
                            <h2>Stefano Napolitani</h2>
                            Via Dante, 22<br>
                            Bressanone, BZ 39042<br>
                            Phone: 123 456 7890<br>
                            <a href="mailto:#">stefano.napolitani@mail.com</a>
                        </address>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div>
            <!-- /#colonne medici -->
            <!--HereMaps-->           
            <div class="HereMaps">
                <hr class="featurette-divider">
                <div class="mappa">
                    <div id="map" style="width: 800px; height: 500px;"></div>
                </div>       
            </div>     
            <!-- /.HereMaps-->

        </div>
        <!-- --------------------------------------- /.contenitore --------------------------------------------- -->

        <!-- ------------------------------------------- footer ------------------------------------------------ -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Servizio Sanitario</h2>
                        <a href="" data-toggle="modal" data-target="#modalInformativa" style="color: inherit">Informativa privacy</a>
                    </div>
                    <div class="col-md-3">
                        <address>
                            <h2>Indirizzi</h2>
                            Bolzano: Via Lorenz Böhler, 5<br>
                            Trento: Largo Medaglie d'oro, 9
                        </address>
                    </div>
                    <div class="col-md-3">
                        <h2>Contatti</h2>
                        Bolzano: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.bz@mail.com</a><br>
                        Trento: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.tn@mail.com</a><br>
                    </div>
                    <div class="col-md-3">
                        <h2>Link utili</h2> 
                        <a href="http://www.salute.gov.it/portale/home.html" style="color: inherit" target="_blank">Servizio Sanitario Nazionale</a><br>
                        <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5073&area=Lea&menu=vuoto" style="color: inherit" target="_blank">I pricìpi del SSN</a><br>
                        <a href="https://www.avis.it/" style="color: inherit" target="_blank">AVIS</a><br>
                    </div>
                </div>
            </div>

            <div>
                <div style="padding: 0px 10%;">
                    <hr style="background-color: white">
                </div>
                <div class="text-center">
                    <p>
                        © Copyright 2019 - All Rights Reserved
                    </p>
                </div>
            </div>                
        </footer>
        <!-- ------------------------------------------- /.footer ----------------------------------------------- -->

        <!-- ------------------------------------------- cookiebar ---------------------------------------------- -->
        <div class="alert text-center cookie_modal hidden" id="cookie_modal" role="alert">
            <b>Ti piacciono i biscotti ?</b> &#x1F36A; Su questa pagina vengo utilizzati cookies per assicurarti la miglior esperienza sul nostro sito web. Proseguendo sul sito accetti anche l'informativa della privacy <a href="" data-toggle="modal" data-target="#modalInformativa">Informativa</a>
            <button type="button" onclick="acceptCookie()" class="btn btn-primary btn-sm" aria-label="Close">
                Accetto
            </button>
        </div>
        <!-- ----------------------------------------- /.cookiebar ---------------------------------------------- -->

        <!-- -------------------------------------------- Modal ------------------------------------------------- -->
        <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="modalLoginFormTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">

                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalLoginFormTitle">Accedi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="LoginServlet" method="POST">
                            <div class="form-group">
                                <label for="InputEmail">Email address</label>
                                <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email" name="username">
                            </div>
                            <div class="form-group">
                                <label for="InputPassword">Password</label>
                                <input type="password" class="form-control" id="InputPassword" placeholder="Password" name="password">
                            </div>
                            <div class="row">
                                <div class="form-group form-check col-lg-6" style="text-align: center;">

                                    <input type="checkbox" name="remember" class="form-check-input"  value="1"/>

                                    <label class="form-check-label" for="Check">Ricordami</label>
                                </div>
                                <div class="radio_login col-lg-6">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioPaziente" name="radioLogin" class="custom-control-input" value="a" checked>
                                        <label class="custom-control-label" for="radioPaziente" >Paziente</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioMedico" name="radioLogin" class="custom-control-input" value="b">
                                        <label class="custom-control-label" for="radioMedico">Medico</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="radioServizio" name="radioLogin" class="custom-control-input" value="c">
                                        <label class="custom-control-label" for="radioServizio">Servizio Sanitario</label>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer justify-content-center">
                                <button type="submit" class="btn btn-primary rounded-pill w-100">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalInformativa" tabindex="-1" role="dialog" aria-labelledby="modalInformativaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">           
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalInformativaTitle">Informativa privacy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding-left: 10%; padding-right: 10%">   
                        <p style="text-align: justify">
                            Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. 
                            Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, 
                            quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy 
                            e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale 
                            diffusione a terzi se necessaria per l'erogazione di un servizio. Ogni immagine di profilo caricata sarà di dominio pubblico e visibile chiunque.
                            I cookies utilizzati sono necessari per la funzionalità del remember me. Per la mappa è utilizzato il servizio di terzi HEREmaps, per le loro
                            informazioni della privacy si rimanda al loro <a href="https://legal.here.com/it-it/privacy/policy" target="_blank">sito</a>.
                        </p>
                        <div class="modal-footer ">                          
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Accetto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ------------------------------------------- /.modal ------------------------------------------------ -->
        <!-- --------------------------------------------- toast ------------------------------------------------ -->
        <div class="toast" id="myToast" role="alert" aria-live="assertive" aria-atomic="true" style="position: absolute; top: 56px; right: 0; z-index: 10;">
            <div class="toast-header" style="background-color: rgba(255, 255, 0, 0.2)">
                <strong class="mr-auto">Errore</strong>
                <small>Adesso</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body" style="padding-right: 150px; background-color: rgba(255, 255, 0, 0.3)">
                Il login è errato, ritentare.
            </div>
        </div>
        <!-- ------------------------------------------- /.toast ------------------------------------------------ -->

        <!-- ------------------------------------------ JavaScript --------------------------------------------- -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main-end.js"></script>

        <% if (errore != null) { %>
            <% if (errore == 1) { %>
                <script>
                    $(document).ready(function () {
                        $("#myToast").toast({
                            delay: 10000
                        });
                        $("#myToast").toast('show');
                    });
                </script>
            <% } %>
        <%}%>    
    </body>
    <!-- -------------------------------------------- /.body --------------------------------------------------- -->
</html>