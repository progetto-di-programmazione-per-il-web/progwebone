/*
* HERE Maps
*/

function addMarkersToMap(map) {
    var bolzanoMarker = new H.map.Marker({lat:46.4950752, lng:11.3367912});
    map.addObject(bolzanoMarker);

    var meranoMarker = new H.map.Marker({lat:46.665148, lng: 11.1587052});
    map.addObject(meranoMarker);

    var trentoMarker = new H.map.Marker({lat:46.0759718, lng:11.1222863});
    map.addObject(trentoMarker);

    var roveretoMarker = new H.map.Marker({lat:45.8908106, lng: 11.0405178});
    map.addObject(roveretoMarker);

    var bressanoneMarker = new H.map.Marker({lat:46.71845, lng:11.6475513});
    map.addObject(bressanoneMarker);
}

var platform = new H.service.Platform({
    'apikey': 'JXPP2V-PTqNPo7rcvBna_v-YnSbbXprrBEN8cPIROf8'
});

var maptypes = platform.createDefaultLayers();
var map = new H.Map(
    document.getElementById('map'),
    maptypes.vector.normal.map,
    {
        center: {lat:46.348204, lng:11.2904099},
        zoom: 8,
        pixelRatio: window.devicePixelRatio || 1
    }
);

// add a resize listener to make sure that the map occupies the whole container
window.addEventListener('resize', () => map.getViewPort().resize());

//Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
  
// Create the default UI components
var ui = H.ui.UI.createDefault(map, maptypes);
  
// Now use the map as required...
window.onload = function(){
    addMarkersToMap(map);
}

//----------------------------------------------------------------------------------------------------

/*
* Cookie Alert
*/

var cookie_modal = document.getElementById("cookie_modal");

// START IGNORE FOLLOWING LINES
// DO NOT MODIFY THE CODE BELOW

var domIsReady = (function(domIsReady) {
    var isBrowserIeOrNot = function() {
        return (!document.attachEvent || typeof document.attachEvent === "undefined" ? 'not-ie' : 'ie');
    }

    domIsReady = function(callback) {
        if(callback && typeof callback === 'function'){
            if(isBrowserIeOrNot() !== 'ie') {
                document.addEventListener("DOMContentLoaded", function() {
                    return callback();
                });
            } else {
                document.attachEvent("onreadystatechange", function() {
                    if(document.readyState === "complete") {
                        console.log("old browser - ie");
                        return callback();
                    }
                });
            }
        } else {
            console.error('The callback is not a function!');
        }
    }
    return domIsReady;
})(domIsReady || {});
//END IGNORE

//on document ready, do something : 
(function(document, window, domIsReady, undefined) {
    domIsReady(function() {
        // qui va messo il codice/funzioni che vengnon eseguite subito dopo il caricamento della pagina
        checkCookieConsent();
    });
})(document, window, domIsReady);

            
//function that checks : if cookieConsent is false -> show cookie modal
function checkCookieConsent() {
    console.log("checking_cookie_consent");
    if(getCookie('cookieInfo') != null){                            //se il cookie esiste
        console.log("cookie_consent", "exists");						
        if(getCookie('cookieInfo') == 'true'){				        //se il cookie è stato accettato -> ok, non fare niente
            console.log("cookie_consent_value:", true);
        }
    }else{
        setTimeout(function(){ 
            toggleCookieModal();  
        }, 2000);                                                   //remove class hidden, and show cookie modal
    }
}


function toggleCookieModal(){
    toggleClass(cookie_modal, "hidden");    //remove class hidden (fai vedere il modale)
}

function acceptCookie(){
    setCookie('cookieInfo', true, 30);      //Set cookie to true
    console.log("cookie_consent:", true);
    toggleClass(cookie_modal, "hidden");    //toggleClass hidden (remove it, hide cookie consent)
}

//add class to el if el doesnt alredy have class, or remove class from el if it alredy has class
function toggleClass(el, className){
    el.classList.toggle(className);
}

//DEFAULT/GENERIC MANAGE COOKIES FUNCTIONS 
function setCookie(name, value, days) {
    var expires = "";
    
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }

    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    
    return null;
}

function removeCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}