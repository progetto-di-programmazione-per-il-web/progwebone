<%@page import="it.unitn.disi.wp.progwebone.entities.Paziente"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.EsamePossibile"%>
<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Visita"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Medico"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Ricetta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Titolo -->
        <title>Home medico</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" href="css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    </head>

    <%
        Medico tempMedico = (Medico) request.getSession(false).getAttribute("user");
        List<Visita> visite = (List<Visita>) request.getAttribute("LISTA_VISITE");
        List<Ricetta> ricette = (List<Ricetta>) request.getAttribute("LISTA_RICETTE");
        List<EsamePossibile> esami_possibili = (List<EsamePossibile>) request.getAttribute("ESAMI_POSSIBILI");
        List<Paziente> pazienti = (List<Paziente>) request.getAttribute("LISTA_PAZIENTI");
        String ora_visita;
        String ora_ricetta;

        if(visite.size() != 0){
            Visita ult_visita = visite.get(0);
            ora_visita = String.valueOf(ult_visita.getTimestamp()).substring(0, 19);
        }else{
            ora_visita = "ancora nessuna visita effettuata";
        }
        if(ricette.size() != 0){
            Ricetta ult_ricetta = ricette.get(0);
            ora_ricetta= String.valueOf(ult_ricetta.getTimestamp()).substring(0, 19);
        }else{
            ora_ricetta = "ancora nessuna ricetta prescritta";
        }
        
        Integer codice = (Integer) request.getSession().getAttribute("codice");
        request.getSession().removeAttribute("codice");
        
    %>

    <body>
        <!-- ---------------------------------------- Navbar ------------------------------------------------ -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <span class="navbar-brand">Servizio Sanitario</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home medico<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <a href="LogoutServlet"><button class="btn btn-outline-info my-2 my-sm-0" type="button">Logout</button></a>
            </div>
        </nav>
        <!-- ------------------------------------------ /.navbar -------------------------------------------- -->

        <!-- ---------------------------------------- contenitore --------------------------------------------- -->

        <div class="contenitore" style="padding-top: 91px;">
            
            <h2 id="buongiorno">Buongiorno, <%= tempMedico.getNome()%> <%= tempMedico.getCognome()%></h2>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <p>Ultima visita effettuata: <%= ora_visita %></p>
                    <p>Ultima ricetta prescritta: <%= ora_ricetta %></p>
                </div>
                <div class="col-md-6">
                    
                    <form action="DatiPaziente">
                        <label for="autocomplete">Seleziona un paziente:</label>
                        <select class="js-data-example-ajax" id="autocomplete" name="id" style="width: 50%">
                            <% for(Paziente tempPaziente : pazienti){ %>
                                <option value="<%= tempPaziente.getId() %>"><%= tempPaziente.getNome() %> <%= tempPaziente.getCognome()%></option>
                            <% } request.removeAttribute("LISTA_PAZIENTI"); %>
                        </select>
                        <button type="submit" class="btn btn-secondary">Cerca</button>
                    </form>
                    
                </div>
            </div>
            <br>
            <div class="card border-info">
                <h2 class="card-header bg-info">Visite mediche effettuate</h2>
                <div class="card-body">
                    <div id="tabella">
                        <table class="display no-wrap" width="100%" id="tabella_ultime_visite">
                            <thead>
                                <tr>
                                    <th>ID Visita</th>
                                    <th>ID Medico</th>
                                    <th>ID Paziente</th>
                                    <th data-priority="1">Visita</th>
                                    <th data-priority="2">Esame prescritto</th>
                                    <th>Data e ora</th>
                                    <th>Pagata</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <% for(Visita tempVisite : visite){ %>
        
                                <tr>
                                    <td> <%= tempVisite.getId_visita() %> </td>
                                    <td> <%= tempVisite.getId_medico() %> </td>
                                    <td> <%= tempVisite.getId_paziente() %> </td>
                                    <td> <%= tempVisite.getVisita() %> </td>
                                    <td> <%= tempVisite.getEsame_prescritto() %> </td>
                                    <td> <%= String.valueOf(tempVisite.getTimestamp()).substring(0, 19) %> </td>
                                    <td> <%= tempVisite.getPagato() %> </td>
                                </tr>
        
                                <% } request.removeAttribute("LISTA_VISITE"); %>
        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="card border-info">
                <h2 class="card-header bg-info">Ricette prescritte</h2>
                <div class="card-body">
                    <div id="tabella">
                        <table class="display no-wrap" width="100%" id="tabella_ultime_ricette">
                            <thead>
                                <tr>
                                    <th data-priority="1">ID Ricetta</th>
                                    <th>ID Medico</th>
                                    <th>ID Paziente</th>
                                    <th data-priority="2">Farmaco</th>
                                    <th>Data e ora</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <% for(Ricetta tempRicette : ricette){ %>
        
                                <tr>
                                    <td> <%= tempRicette.getId_ricetta() %> </td>
                                    <td> <%= tempRicette.getId_medico() %> </td>
                                    <td> <%= tempRicette.getId_paziente() %> </td>
                                    <td> <%= tempRicette.getFarmaco() %> </td>
                                    <td> <%= String.valueOf(tempRicette.getTimestamp()).substring(0, 19) %> </td>
                                </tr>
        
                                <% } request.removeAttribute("LISTA_RICETTE"); %>
        
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="opzioni_medico">
                <div class="row">
                    <div class="col-sm-6" id="visualizzaPazienti">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Visualizza pazienti</h5>
                                <p class="card-text">Clicca qua per visualizzare la lista di tutti i pazienti</p>
                                <form action="ListaPazienti" method="POST">
                                    <button class="btn btn-primary" type="submit">Visualizza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Esegui visita o prescrivi farmaco</h5>
                                <p class="card-text">Se sai già i dati del paziente clicca sui seguenti bottoni per eseguire una visita o prescrivere un farmaco più velocemente</p>                    
                                <button class="btn btn-primary" id="bottoneVisita" type="submit" data-toggle="modal" data-target="#modalVisita">Esegui visita</button>                       
                                <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#modalFarmaco">Prescrivi farmaco</button>                   
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
        


        </div>
        <!-- --------------------------------------- /.contenitore --------------------------------------------- -->

        <!-- -------------------------------------------- Modal ------------------------------------------------- -->
        <div class="modal fade" id="modalFarmaco" tabindex="-1" role="dialog" aria-labelledby="modalFarmacoTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">         
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalFarmacoTitle">Prescrivi farmaco</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="AddFarmacoVeloceServlet" method="POST">                          
                            <div class="form-group">
                                <label for="InputNome">Nome paziente</label>
                                <input type="text" class="form-control" id="InputNome" placeholder="Nome" name="Nome" required>
                            </div>
                            <div class="form-group">
                                <label for="InputCognome">Cognome paziente</label>
                                <input type="text" class="form-control" id="InputCognome" placeholder="Cognome" name="Cognome" required>
                            </div>
                            <div class="form-group">
                                <label for="InputFiscale">Codice fiscale</label>
                                <input type="text" class="form-control" id="InputFiscale" placeholder="Codice fiscale" name="Codice_Fiscale" required>
                            </div>                         
                            <div class="form-group">
                                <label for="Farmaco">Farmaco da prescrivere</label>
                                <input type="text" class="form-control" id="Farmaco" placeholder="Farmaco" name="Farmaco" required>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-outline-success">Prescrivi</button>
                                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Annulla</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalVisita" tabindex="-1" role="dialog" aria-labelledby="modalVisitaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">         
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalVisitaTitle">Esegui visita</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="AddVisitaVeloceServlet" method="POST">                          
                            <div class="form-group">
                                <label for="InputNome">Nome paziente</label>
                                <input type="text" class="form-control" id="InputNome" placeholder="Nome" name="Nome" required>
                            </div>
                            <div class="form-group">
                                <label for="InputCognome">Cognome paziente</label>
                                <input type="text" class="form-control" id="InputCognome" placeholder="Cognome" name="Cognome" required>
                            </div>
                            <div class="form-group">
                                <label for="InputFiscale">Codice fiscale</label>
                                <input type="text" class="form-control" id="InputFiscale" placeholder="Codice fiscale" name="Codice_Fiscale" required>
                            </div>
                            <div class="form-group">
                                <label for="Visita">Visita</label>
                                <input type="text" class="form-control" id="Visita" placeholder="Visita" name="Visita" required>
                            </div>                      
                            <div class="form-group">
                                <label for="Esame_prescritto">Esame prescritto</label>
                                <select class="custom-select" id="Esame_prescritto" name="Esame_prescritto" required>
                                    <option value="-1">Nessuno</option>
                                    
                                    <% for(EsamePossibile esaTmp : esami_possibili){ %>
                                        <option value="<%= esaTmp.getId_esame()%>" ><%= esaTmp.getEsame()%></option>
                                    <% }%>
                                     
                                </select>
                            </div>
                            <div class="modal-footer ">
                                <button type="submit" class="btn btn-outline-success">Esegui</button>
                                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Annulla</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalInformativa" tabindex="-1" role="dialog" aria-labelledby="modalInformativaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">           
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalInformativaTitle">Informativa privacy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding-left: 10%; padding-right: 10%">   
                        <p style="text-align: justify">
                            Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. 
                            Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, 
                            quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy 
                            e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale 
                            diffusione a terzi se necessaria per l'erogazione di un servizio. Ogni immagine di profilo caricata sarà di dominio pubblico e vibile chiunque.
                            I cookies utilizzati sono necessari per la funzionalità del remember me. Per la mappa è utilizzato il servizio di terzi HEREmaps, per le loro
                            informazioni della privacy si rimanda al loro <a href="https://legal.here.com/it-it/privacy/policy" target="_blank">sito</a>.
                        </p>
                        <div class="modal-footer ">                          
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Accetto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ------------------------------------------- /.modal ------------------------------------------------ -->

        <!-- ------------------------------------------- footer ------------------------------------------------ -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Servizio Sanitario</h2>
                        <a href="" data-toggle="modal" data-target="#modalInformativa" style="color: inherit">Informativa privacy</a>
                    </div>
                    <div class="col-md-3">
                        <address>
                            <h2>Indirizzi</h2>
                            Bolzano: Via Lorenz Böhler, 5<br>
                            Trento: Largo Medaglie d'oro, 9
                        </address>
                    </div>
                    <div class="col-md-3">
                        <h2>Contatti</h2>
                        Bolzano: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.bz@mail.com</a><br>
                        Trento: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.tn@mail.com</a><br>
                    </div>
                    <div class="col-md-3">
                        <h2>Link utili</h2> 
                        <a href="http://www.salute.gov.it/portale/home.html" style="color: inherit">Servizio Sanitario Nazionale</a><br>
                        <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5073&area=Lea&menu=vuoto" style="color: inherit">I pricìpi del SSN</a><br>
                        <a href="https://www.avis.it/" style="color: inherit">AVIS</a><br>
                    </div>
                </div>
            </div>

            <div>
                <div style="padding: 0px 10%;">
                    <hr style="background-color: white">
                </div>
                <div class="text-center">
                    <p>
                        © Copyright 2019 - All Rights Reserved
                    </p>
                </div>
            </div>                
        </footer>
        <!-- ------------------------------------------- /.footer ----------------------------------------------- -->
        <!-- --------------------------------------------- toast ------------------------------------------------ -->
        <div class="toast" id="toastErrore" role="alert" aria-live="assertive" aria-atomic="true" style="position: absolute; top: 56px; right: 0; z-index: 10;">
            <div class="toast-header" style="background-color: #f87a84">
                <strong class="mr-auto" style="color: black">Errore</strong>
                <small>Adesso</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body" style="background-color: #f8d7da">
                Operazione non avvenuta, inserire i dati correttamente.
            </div>
        </div>
        <div class="toast" id="ToastSuccesso" role="alert" aria-live="assertive" aria-atomic="true" style="position: absolute; top: 56px; right: 0; z-index: 10;">
            <div class="toast-header" style="background-color: #78df90">
                <strong class="mr-auto" style="color: black">Successo</strong>
                <small>Adesso</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body" style="padding-right: 150px; background-color: #d4edda">
                L'operazione è avvenuta con successo.
            </div>
        </div>
        <!-- ------------------------------------------- /.toast ------------------------------------------------ -->

        <!-- ------------------------------------------ JavaScript --------------------------------------------- -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/select2.min.js"></script>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script src="js/main-end.js"></script>
        <script>
            $(document).ready(function() {
                $('#tabella_ultime_visite').DataTable({
                    responsive: true,
                    "order": [[ 5, "desc" ]]
                });
                $('#tabella_ultime_ricette').DataTable({
                    responsive: true,
                    "order": [[ 4, "desc" ]]
                });
                $("#autocomplete").select2({
                    placeholder: 'Seleziona un paziente',
                });
            });
        </script>
        <% if (codice != null) { %>
            <% if (codice == 1) { %>
                <script>
                    $(document).ready(function () {
                        $("#toastErrore").toast({
                            delay: 10000
                        });
                        $("#toastErrore").toast('show');
                    });
                </script>
            <% }else{ %>
                <script>
                    $(document).ready(function () {
                        $("#ToastSuccesso").toast({
                            delay: 10000
                        });
                        $("#ToastSuccesso").toast('show');
                    });
                </script>
        <% } }%>  
    </body>
</html>
