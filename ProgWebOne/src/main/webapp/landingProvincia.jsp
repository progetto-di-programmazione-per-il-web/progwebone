<%@page import="it.unitn.disi.wp.progwebone.entities.ServizioSanitarioProvinciale"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Paziente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.util.*" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Titolo -->
        <title>Home servizio sanitario</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/gijgo.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    </head>

    <%
        ServizioSanitarioProvinciale tempServizio = (ServizioSanitarioProvinciale) request.getSession(false).getAttribute("user");
        //recupero i pazienti (mandato dalla servlet)
        List<Paziente> pazienti = (List<Paziente>) request.getAttribute("LISTA_PAZIENTI"); 
    %>

    <body>
        <!-- ---------------------------------------- Navbar ------------------------------------------------ -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <span class="navbar-brand">Servizio Sanitario</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home servizio sanitario<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <a href="LogoutServlet"><button class="btn btn-outline-info my-2 my-sm-0" type="button">Logout</button></a>
            </div>
        </nav>
        <!-- ------------------------------------------ /.navbar -------------------------------------------- -->

        <!-- ---------------------------------------- contenitore --------------------------------------------- -->

        <div class="contenitore" style="padding-top: 91px;">
            <h2 id="buongiorno"> <%= tempServizio.getNome() %> </h2>
            <div class="row">
                <div class="col-md-6" id="reportSpese">
                    <a href="ReportSpese">Per avere un report delle prestazioni erogate clicca qua.</a>
                </div>
                <div class="col-md-6">
                    <form action="xlsMakerServlet" method="POST">
                        <div class="form-group">
                            <label for="datepicker">Seleziona una data per avere un report XLS delle medicine erogate</label>
                            <input id="datepicker" type="text" class="form-control" width="276" name="data"/>
                        </div>
                        <button type="submit" class="btn btn-success" id="scarica_xls">Scarica</button>                       
                    </form>
                </div>
            </div>
            
            <br>       
            
            <h2>Lista Pazienti</h2>
                
                    <table class="display no-wrap" id="tabella_pazienti" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th data-priority="1">Cognome</th>
                                <th>Sesso</th>
                                <th>Codice fiscale</th>
                                <th>Data di nascita</th>
                                <th>Luogo di nascita</th>
                                <th>Provincia</th>
                                <th>e-mail</th>
                                <th data-priority="2"></th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            
                            <% for(Paziente tempPaziente : pazienti){ %>

                            <tr>                     
                                <td> <%= tempPaziente.getId()%> </td>
                                <td> <%= tempPaziente.getNome() %> </td>
                                <td> <%= tempPaziente.getCognome()%> </td>
                                <td> <%= tempPaziente.getSesso()%> </td>
                                <td> <%= tempPaziente.getCodiceFiscale()%> </td>
                                <td> <%= tempPaziente.getDataDiNascita()%> </td>
                                <td> <%= tempPaziente.getLuogoDiNascita()%> </td>
                                <td> <%= tempPaziente.getProvincia()%> </td>
                                <td> <%= tempPaziente.getEmail()%> </td>
                                <td><a href="DatiPaziente?id=<%= tempPaziente.getId()%>" title="Vedi paziente"><i class="far fa-eye fa-lg"></i></a></td>
                            </tr>

                            <% } request.removeAttribute("LISTA_PAZIENTI"); %>

                        </tbody>
                    </table>
            
        </div>
        <!-- --------------------------------------- /.contenitore --------------------------------------------- -->

        <!-- ------------------------------------------- footer ------------------------------------------------ -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Servizio Sanitario</h2>
                        <a href="" data-toggle="modal" data-target="#modalInformativa" style="color: inherit">Informativa privacy</a>
                    </div>
                    <div class="col-md-3">
                        <address>
                            <h2>Indirizzi</h2>
                            Bolzano: Via Lorenz Böhler, 5<br>
                            Trento: Largo Medaglie d'oro, 9
                        </address>
                    </div>
                    <div class="col-md-3">
                        <h2>Contatti</h2>
                        Bolzano: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.bz@mail.com</a><br>
                        Trento: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.tn@mail.com</a><br>
                    </div>
                    <div class="col-md-3">
                        <h2>Link utili</h2> 
                        <a href="http://www.salute.gov.it/portale/home.html" style="color: inherit">Servizio Sanitario Nazionale</a><br>
                        <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5073&area=Lea&menu=vuoto" style="color: inherit">I pricìpi del SSN</a><br>
                        <a href="https://www.avis.it/" style="color: inherit">AVIS</a><br>
                    </div>
                </div>
            </div>

            <div>
                <div style="padding: 0px 10%;">
                    <hr style="background-color: white">
                </div>
                <div class="text-center">
                    <p>
                        © Copyright 2019 - All Rights Reserved
                    </p>
                </div>
            </div>                
        </footer>
        <!-- ------------------------------------------- /.footer ----------------------------------------------- -->        
        <!-- -------------------------------------------- Modal ------------------------------------------------- -->
        <div class="modal fade" id="modalInformativa" tabindex="-1" role="dialog" aria-labelledby="modalInformativaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">           
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalInformativaTitle">Informativa privacy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding-left: 10%; padding-right: 10%">   
                        <p style="text-align: justify">
                            Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. 
                            Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, 
                            quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy 
                            e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale 
                            diffusione a terzi se necessaria per l'erogazione di un servizio. Ogni immagine di profilo caricata sarà di dominio pubblico e vibile chiunque.
                            I cookies utilizzati sono necessari per la funzionalità del remember me. Per la mappa è utilizzato il servizio di terzi HEREmaps, per le loro
                            informazioni della privacy si rimanda al loro <a href="https://legal.here.com/it-it/privacy/policy" target="_blank">sito</a>.
                        </p>
                        <div class="modal-footer ">                          
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Accetto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ------------------------------------------- /.modal ------------------------------------------------ -->
        <!-- ------------------------------------------ JavaScript --------------------------------------------- -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script src="js/main-end.js"></script>
        <script>
            $(document).ready(function() {
                $('#tabella_pazienti').DataTable({
                    responsive: true
                });
            });
            
            //datePicker
            var d = new Date();
            var n = d.toISOString();
            n = n.substring(0, 10);
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd',
                value: n
            });      
        </script>
   
    </body>
</html>
