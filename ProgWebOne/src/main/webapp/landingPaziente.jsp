<%@page import="it.unitn.disi.wp.progwebone.entities.Notifiche"%>
<%@page import="java.util.List"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Medico"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Paziente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Titolo -->
        <title>Home paziente</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
    </head>

    <%
        Paziente tempPaziente = (Paziente) request.getSession(false).getAttribute("user");
        List<Medico> listaMedici = (List<Medico>) request.getSession(false).getAttribute("lista_medici");
        Medico medPaziente = (Medico) request.getSession(false).getAttribute("medico");
        Notifiche notifica = (Notifiche) request.getSession(false).getAttribute("notifica");

        int totale = notifica.getNumero_esami() + notifica.getNumero_visite() + notifica.getNumero_ricette();
    %>           



    <body>
        <!-- ---------------------------------------- Navbar ------------------------------------------------ -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <span class="navbar-brand">Servizio Sanitario</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home paziente<span class="sr-only">(current)</span></a>
                    </li>
                </ul>

                <a tabindex="0" class="btn btn-outline-light" id="notifiche" style="margin-right: 50px; margin-left: 50px; cursor: pointer" role="button" data-toggle="popover" data-trigger="focus" data-placement="bottom" title="Notifiche" data-html="true" data-content="Nuovi esami da visualizzare: <%= notifica.getNumero_esami()%><br>Nuove visite da visualizzare: <%= notifica.getNumero_visite()%><br>Nuove ricette da visualizzare: <%= notifica.getNumero_ricette()%>"><%= totale%><i class="fas fa-bell bell" style="padding-left: 20px" ></i></a>

                <a href="LogoutServlet"><button class="btn btn-outline-info my-2 my-sm-0" type="button">Logout</button></a>
            </div>
        </nav>
        <!-- ------------------------------------------ /.navbar -------------------------------------------- -->

        <!-- ---------------------------------------- contenitore --------------------------------------------- -->
        <div class="contenitore" style="padding-top: 91px;">

            <h2 id="buongiorno">Buongiorno, <%= tempPaziente.getNome()%> <%= tempPaziente.getCognome()%></h2>
            <br>
            <div class="card">
                <h4 class="card-header">Dati paziente</h4>
                <div class="card-body">
                    <div class="row" id="riga_dati" style="text-align: left">
                        <div class="col-lg-3 colonna_3" id="colonna_dati_paziente_3">
                            <img src="./img/avatar/<%= tempPaziente.getId() %>/<%= tempPaziente.getAvatar_path()%>" class="bd-placeholder-img rounded-circle" width="200px" height="200px" id="img_paziente" alt="avatar">
                        </div>
                        <div class="col-lg-9" id="colonna_dati_paziente_9">
                            <div class="row">
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Nome: <%= tempPaziente.getNome()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Cognome: <%= tempPaziente.getCognome()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Sesso: <%= tempPaziente.getSesso()%></p>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Codice fiscale: <%= tempPaziente.getCodiceFiscale()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Data di nascita: <%= tempPaziente.getDataDiNascita()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Luogo di nascita: <%= tempPaziente.getLuogoDiNascita()%></p>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Provincia: <%= tempPaziente.getProvincia()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>Medico di base:  <%= medPaziente.getCognome() + " " + medPaziente.getNome()%></p>
                                </div>
                                <div class="col-sm-4" id="colonna_dati_paziente">
                                    <p>E-mail: <%= tempPaziente.getEmail()%></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <button class="btn btn-primary" type="submit" data-toggle="modal" data-target="#modalModificaDati">Modifica profilo</button>
                </div>
            </div>
            <br>

            <div class="opzioni_pazienti">
                <div class="row">
                    <div class="col-md-4" id="visualizzaEsami">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Visualizza esami</h5>
                                <p class="card-text">Clicca qua per visualizzare la lista degli esami fatti e da fare</p>
                                <form action="ListaEsami" method="POST">
                                    <button class="btn btn-primary" type="submit">Visualizza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="visualizzaVisite">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Visualizza visite</h5>
                                <p class="card-text">Clicca qua per visualizzare la lista delle visite effettuate dal tuo medico di base</p>
                                <form action="ListaVisite" method="POST">
                                    <button class="btn btn-primary" type="submit">Visualizza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Visualizza ricette</h5>
                                <p class="card-text">Clicca qua per visualizzare la lista delle ricette prescritte</p>
                                <form action="ListaRicette" method="POST">
                                    <button class="btn btn-primary" type="submit">Visualizza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Visualizza ticket</h5>
                                <p class="card-text">Clicca qua per visualizzare la lista dei ticket pagati</p>
                                <form action="ListaTicketPagati" method="POST">
                                    <button class="btn btn-primary" type="submit">Visualizza</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- --------------------------------------- /.contenitore --------------------------------------------- -->

        <!-- -------------------------------------------- Modal ------------------------------------------------- -->
        <div class="modal fade" id="modalModificaDati" tabindex="-1" role="dialog" aria-labelledby="modalModificaDatiTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">

                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalModificaDatiTitle">Modifica Dati</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="ModificaDatiServlet" method="POST" >
                            <div class="form-group">
                                <label for="EmailMod">Indirizzo e-mail</label>
                                <input type="email" class="form-control" id="EmailMod" aria-describedby="emailHelp" placeholder="Enter email" value="<%= tempPaziente.getEmail()%>" name="Email" required>
                            </div>
                            <div class="form-group">
                                <label for="medicoDiBaseMod"  >Medico di base</label>
                                <select class="custom-select" id="medicoDiBaseMod" name="medicoDiBaseMod" >
                                    <!-- inizio ciclo medici della provincia del paziente-->

                                    <option value="-1" > &LT;Seleziona Medico&GT; </option>

                                    <% for (Medico medTmp : listaMedici) {%>
                                    <option value="<%= medTmp.getId_medico()%>" ><%= medTmp.getCognome() + " " + medTmp.getNome()%></option>
                                    <% }%>
                                    <!-- fine del ciclo -->
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="password_mod">Password</label>
                                <input type="password" class="form-control" id="password_mod" placeholder="Enter new password" value="nopass" name="Password" aria-describedby="passwordHelpBlock" required>
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    La password deve essere completamente riscritta per essere modificata correttamente.
                                </small>
                            </div>
                            <div class="form-group">
                                <label for="password_mod2">Inserire nuovamente la password</label>
                                <input type="password" class="form-control" id="password_mod2" placeholder="Enter new password" value="nopass" name="Password" required>
                                <div class="invalid-feedback">
                                    Le password non corrispondono
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-outline-success" id="btn_salva">Salva</button>
                                <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Annulla</button>
                            </div>
                        </form>
                        <hr>
                        <form action="UploadImg" method="POST" enctype="multipart/form-data">  
                            <div class="form-group">                                    
                                <div class="custom-file" style="padding-bottom: 50px">
                                    <input type="file" class="custom-file-input" name="avatar" id="avatar" placeholder="Avatar" accept="image/png, image/jpeg" required>
                                    <label class="custom-file-label" for="avatar">Foto profilo</label>
                                </div>

                                <button type="submit" class="btn btn-outline-primary">Carica</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalInformativa" tabindex="-1" role="dialog" aria-labelledby="modalInformativaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">           
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalInformativaTitle">Informativa privacy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding-left: 10%; padding-right: 10%">   
                        <p style="text-align: justify">
                            Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. 
                            Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, 
                            quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy 
                            e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale 
                            diffusione a terzi se necessaria per l'erogazione di un servizio. Ogni immagine di profilo caricata sarà di dominio pubblico e vibile chiunque.
                            I cookies utilizzati sono necessari per la funzionalità del remember me. Per la mappa è utilizzato il servizio di terzi HEREmaps, per le loro
                            informazioni della privacy si rimanda al loro <a href="https://legal.here.com/it-it/privacy/policy" target="_blank">sito</a>.
                        </p>
                        <div class="modal-footer ">                          
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Accetto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ------------------------------------------- /.modal ------------------------------------------------ -->

        <!-- ------------------------------------------- footer ------------------------------------------------ -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Servizio Sanitario</h2>
                        <a href="" data-toggle="modal" data-target="#modalInformativa" style="color: inherit">Informativa privacy</a>
                    </div>
                    <div class="col-md-3">
                        <address>
                            <h2>Indirizzi</h2>
                            Bolzano: Via Lorenz Böhler, 5<br>
                            Trento: Largo Medaglie d'oro, 9
                        </address>
                    </div>
                    <div class="col-md-3">
                        <h2>Contatti</h2>
                        Bolzano: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.bz@mail.com</a><br>
                        Trento: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.tn@mail.com</a><br>
                    </div>
                    <div class="col-md-3">
                        <h2>Link utili</h2> 
                        <a href="http://www.salute.gov.it/portale/home.html" style="color: inherit">Servizio Sanitario Nazionale</a><br>
                        <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5073&area=Lea&menu=vuoto" style="color: inherit">I pricìpi del SSN</a><br>
                        <a href="https://www.avis.it/" style="color: inherit">AVIS</a><br>
                    </div>
                </div>
            </div>

            <div>
                <div style="padding: 0px 10%;">
                    <hr style="background-color: white">
                </div>
                <div class="text-center">
                    <p>
                        © Copyright 2019 - All Rights Reserved
                    </p>
                </div>
            </div>                
        </footer>
        <!-- ------------------------------------------- /.footer ----------------------------------------------- -->        

        <!-- ------------------------------------------ JavaScript --------------------------------------------- -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main-end.js"></script>
        <script>
            $("input[type=password]").keyup(function () {
                if ($("#password_mod").val() == $("#password_mod2").val()) {
                    $("#password_mod2").removeClass("is-invalid");
                    $("#btn_salva").prop('disabled', false);
                } else {
                    $("#password_mod2").addClass("is-invalid");
                    $("#btn_salva").prop('disabled', true);
                }
            });

            $(document).ready(function () {
                $('[data-toggle="popover"]').popover();
            });
        </script>

        <% if (notifica.getNumero_esami() != 0 || notifica.getNumero_visite() != 0 || notifica.getNumero_ricette() != 0) { %>
        <script>
            $(document).ready(function () {
                $(".bell").css("animation-name", "ring");
            });
        </script>
        <% } else { %>
        <script>
            $(document).ready(function () {
                $(".bell").css("animation-name", "none");
            });
        </script>
        <% }%>

    </body>
</html>
