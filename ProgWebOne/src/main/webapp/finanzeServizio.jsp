<%@page import="it.unitn.disi.wp.progwebone.entities.Esame"%>
<%@page import="it.unitn.disi.wp.progwebone.entities.Visita"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import= "java.util.*" %>

<!DOCTYPE html>
<html>
    <head>
       <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Titolo -->
        <title>Finanze</title>
        
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/all.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css">
        
    </head>

    <%
        //recupero le visite, gli esami e le ricette (dalla servlet)
        List<Visita> visite = (List<Visita>) request.getAttribute("Lista_visite");
        List<Esame> esami = (List<Esame>) request.getAttribute("Lista_esami");
        int totale = (int) request.getAttribute("totale");
    %>

     <body>
        <!-- ---------------------------------------- Navbar ------------------------------------------------ -->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
            <span class="navbar-brand">Servizio Sanitario</span>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
                          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="LandingServizio">Home servizio sanitario<span class="sr-only"></span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Finanze<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                <!-- Bottone logout -->
                <a href="LogoutServlet"><button class="btn btn-outline-info my-2 my-sm-0" type="button">Logout</button></a>          
            </div>
        </nav>
        <!-- ------------------------------------------ /.navbar -------------------------------------------- -->

        <!-- ---------------------------------------- contenitore --------------------------------------------- -->
        <div class="contenitore" style="padding-top: 91px;">
            
            <h2>Lista di visite e esami pagati in provincia</h2>
            <p>La spesa totale in questa provincia è di <%= totale %>€</p>
            <div class="card">
                <h2 class="card-header">Visite mediche</h2>
                <div class="card-body">
                    <div id="tabella">
                        <table class="display no-wrap" width="100%" id="tabella_visite">
                            <thead>
                                <tr>
                                    <th>ID Visita</th>
                                    <th>ID Medico</th>
                                    <th data-priority="1">Visita</th>
                                    <th data-priority="2">Esame prescritto</th>
                                    <th>Data e ora</th>
                                    <th>Pagato</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <% for(Visita tempVisite : visite){ %>
    
                                <tr>
                                    <td> <%= tempVisite.getId_visita() %> </td>
                                    <td> <%= tempVisite.getId_medico() %> </td>
                                    <td> <%= tempVisite.getVisita() %> </td>
                                    <td> <%= tempVisite.getEsame_prescritto() %> </td>
                                    <td> <%= String.valueOf(tempVisite.getTimestamp()).substring(0, 19) %> </td>
                                    <td> <%= tempVisite.getPagato() %> </td>
                                </tr>
    
                                <% } request.removeAttribute("LISTA_VISITE"); %>
    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="card">
                <h2 class="card-header">Esami</h2>
                <div class="card-body">
                    <div id="tabella">
                        <table class="display no-wrap" width="100%" id="tabella_esami">
                            <thead>
                                <tr>
                                    <th>ID Esame</th>
                                    <th>ID Medico</th>
                                    <th data-priority="1">Esame</th>
                                    <th data-priority="2">Esito</th>
                                    <th>Pagato</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                
                                <% for(Esame tempEsami : esami){ %>
    
                                <tr>
                                    <td> <%= tempEsami.getId_esame() %> </td>
                                    <td> <%= tempEsami.getId_medico() %> </td>
                                    <td> <%= tempEsami.getEsame() %> </td>
                                    <td> <%= tempEsami.getEsito() %> </td>
                                    <td> <%= tempEsami.getPagato() %> </td>
                                </tr>
    
                                <% } request.removeAttribute("LISTA_ESAMI"); %>
    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- --------------------------------------- /.contenitore --------------------------------------------- -->
        <!-- -------------------------------------------- Modal ------------------------------------------------- -->
        <div class="modal fade" id="modalInformativa" tabindex="-1" role="dialog" aria-labelledby="modalInformativaTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">           
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <h5 class="modal-title w-100" id="modalInformativaTitle">Informativa privacy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding-left: 10%; padding-right: 10%">   
                        <p style="text-align: justify">
                            Il presente sito tratta i dati prevalentemente in base al consenso degli utenti. 
                            Il conferimento del consenso avviene tramite il banner posto in fondo alla pagina, oppure tramite l’uso o la consultazione del sito, 
                            quale comportamento concludente. Con l'uso o la consultazione del sito i visitatori e gli utenti approvano la presente informativa privacy 
                            e acconsentono al trattamento dei loro dati personali in relazione alle modalità e alle finalità di seguito descritte, compreso l'eventuale 
                            diffusione a terzi se necessaria per l'erogazione di un servizio. Ogni immagine di profilo caricata sarà di dominio pubblico e vibile chiunque.
                            I cookies utilizzati sono necessari per la funzionalità del remember me. Per la mappa è utilizzato il servizio di terzi HEREmaps, per le loro
                            informazioni della privacy si rimanda al loro <a href="https://legal.here.com/it-it/privacy/policy" target="_blank">sito</a>.
                        </p>
                        <div class="modal-footer ">                          
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Accetto</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ------------------------------------------- /.modal ------------------------------------------------ -->

        <!-- ------------------------------------------- footer ------------------------------------------------ -->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <h2>Servizio Sanitario</h2>
                        <a href="" data-toggle="modal" data-target="#modalInformativa" style="color: inherit">Informativa privacy</a>
                    </div>
                    <div class="col-md-3">
                        <address>
                            <h2>Indirizzi</h2>
                            Bolzano: Via Lorenz Böhler, 5<br>
                            Trento: Largo Medaglie d'oro, 9
                        </address>
                    </div>
                    <div class="col-md-3">
                        <h2>Contatti</h2>
                        Bolzano: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.bz@mail.com</a><br>
                        Trento: 123 456 7890<br>
                        <a href="mailto:#">serviziosanitario.tn@mail.com</a><br>
                    </div>
                    <div class="col-md-3">
                        <h2>Link utili</h2> 
                        <a href="http://www.salute.gov.it/portale/home.html" style="color: inherit">Servizio Sanitario Nazionale</a><br>
                        <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5073&area=Lea&menu=vuoto" style="color: inherit">I pricìpi del SSN</a><br>
                        <a href="https://www.avis.it/" style="color: inherit">AVIS</a><br>
                    </div>
                </div>
            </div>
                    
            <div>
                <div style="padding: 0px 10%;">
                    <hr style="background-color: white">
                </div>
                <div class="text-center">
                    <p>
                        © Copyright 2019 - All Rights Reserved
                    </p>
                </div>
            </div>                
        </footer>
        <!-- ------------------------------------------- /.footer ----------------------------------------------- -->        

        <!-- ------------------------------------------ JavaScript --------------------------------------------- -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="DataTables/datatables.min.js"></script>
        <script src="js/main-end.js"></script>
        <script>
            $(document).ready(function() {

                $('#tabella_visite').DataTable({
                    responsive: true              
                });
                $('#tabella_esami').DataTable({
                    responsive: true
                });

            });
        </script>
    </body>
</html>
